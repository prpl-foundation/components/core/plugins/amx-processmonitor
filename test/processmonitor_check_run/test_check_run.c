/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <math.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>

#include "common_functions.h"
#include "processmonitor_functions.h"
#include "test_check_run.h"

int test_setup(void** state) {
    amxut_bus_setup(state);
    resolver_add_all_functions();

    amxd_dm_t* dm = amxut_bus_dm();
    amxo_parser_t* parser = amxut_bus_parser();
    assert_non_null(dm);
    assert_non_null(parser);

    amxd_object_t* root = amxd_dm_get_root(dm);
    assert_non_null(root);

    test_setup_parse_odl(BASE_CONFIG);
    test_setup_parse_odl(DEF_ODL_CONFIG);
    test_setup_parse_odl(MOCK_DEVICE_ODL);

    assert_int_equal(_processmonitor_main(AMXO_START, dm, parser), 0);
    handle_events();

    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:start", NULL);

    return 0;
}

void test_check_test(UNUSED void** state) {
    amxd_dm_t* dm = get_dm();
    amxo_parser_t* parser = processmonitor_get_parser();
    amxb_bus_ctx_t* bus_ctx = amxut_bus_ctx();
    processmonitor_t* pm = pm_get_process_monitor();
    int rv = -1;
    amxc_var_t ret_var;
    amxc_var_t* pm_var = NULL;
    amxc_llist_it_t* test;
    amxd_object_t* test_obj = NULL;
    char* health = NULL;
    unsigned int cycle_duration = 0;

    assert_non_null(dm);
    assert_non_null(parser);
    assert_non_null(bus_ctx);

    set_time(0);

    amxc_var_init(&ret_var);

    amxut_dm_assert_str("ProcessMonitor.Test.1.", "Health", "Awaiting check");

    rv = amxb_get(bus_ctx, "ProcessMonitor.", 0, &ret_var, 3);
    assert_int_equal(rv, 0);
    pm_var = amxc_var_get_first(amxc_var_get_first(&ret_var));
    assert_non_null(pm_var);

    cycle_duration = GET_UINT32(pm_var, "CycleDuration");
    assert_int_equal(cycle_duration, 5);

    set_add_time(cycle_duration);
    amxut_timer_go_to_future_ms(cycle_duration * 1000);

    test = amxc_llist_get_first(&(pm->tests));
    test_obj = ((test_t*) amxc_container_of(test, test_t, it))->object;

    health = amxd_object_get_value(cstring_t, test_obj, "Health", NULL);
    assert_string_equal(health, "Good");

    amxc_var_clean(&ret_var);
    free(health);
}

void test_change_subject_test(UNUSED void** state) {
    amxd_dm_t* dm = get_dm();
    amxo_parser_t* parser = processmonitor_get_parser();
    amxb_bus_ctx_t* bus_ctx = amxut_bus_ctx();
    processmonitor_t* pm = pm_get_process_monitor();
    int rv = -1;
    amxc_var_t ret_var;
    amxc_var_t* pm_var = NULL;
    amxc_llist_it_t* test;
    amxd_object_t* test_obj = NULL;
    char* health = NULL;
    amxd_trans_t trans;
    unsigned int cycle_duration = 0;

    assert_non_null(dm);
    assert_non_null(parser);
    assert_non_null(bus_ctx);

    set_time(0);

    amxc_var_init(&ret_var);

    // trigger call to _pm_test_changed
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "ProcessMonitor.Test.1");
    amxd_trans_set_value(cstring_t, &trans, "Subject", "IP");
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);

    handle_events();

    rv = amxb_get(bus_ctx, "ProcessMonitor.", 0, &ret_var, 3);
    assert_int_equal(rv, 0);
    pm_var = amxc_var_get_first(amxc_var_get_first(&ret_var));
    assert_non_null(pm_var);

    cycle_duration = GET_UINT32(pm_var, "CycleDuration");
    assert_int_equal(cycle_duration, 5);

    set_add_time(cycle_duration);
    amxut_timer_go_to_future_ms(cycle_duration * 1000);

    test = amxc_llist_get_first(&(pm->tests));
    test_obj = ((test_t*) amxc_container_of(test, test_t, it))->object;
    health = amxd_object_get_value(cstring_t, test_obj, "Health", NULL);
    assert_string_equal(health, "Bad");

    amxc_var_clean(&ret_var);
    free(health);
}

void test_change_testinterval_pm(UNUSED void** state) {
    amxd_dm_t* dm = get_dm();
    amxo_parser_t* parser = processmonitor_get_parser();
    amxb_bus_ctx_t* bus_ctx = amxut_bus_ctx();
    processmonitor_t* pm = pm_get_process_monitor();
    int rv = -1;
    amxc_var_t ret_var;
    amxc_var_t* pm_var = NULL;
    amxc_llist_it_t* test;
    amxd_object_t* test_obj = NULL;
    char* health = NULL;
    amxd_trans_t trans;
    unsigned int cycle_duration = 0;

    assert_non_null(dm);
    assert_non_null(parser);
    assert_non_null(bus_ctx);

    set_time(0);

    amxc_var_init(&ret_var);

    // trigger call to pm_write_cycle_duration
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "ProcessMonitor.");
    amxd_trans_set_value(uint32_t, &trans, "CycleDuration", 6);
    amxd_trans_select_pathf(&trans, "ProcessMonitor.Test.1");
    amxd_trans_set_value(cstring_t, &trans, "Subject", "VoiceService");
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);

    handle_events();

    rv = amxb_get(bus_ctx, "ProcessMonitor.", 0, &ret_var, 3);
    assert_int_equal(rv, 0);
    pm_var = amxc_var_get_first(amxc_var_get_first(&ret_var));
    assert_non_null(pm_var);

    cycle_duration = GET_UINT32(pm_var, "CycleDuration");
    assert_int_equal(cycle_duration, 6);

    set_add_time(cycle_duration);
    amxut_timer_go_to_future_ms(cycle_duration * 1000);

    test = amxc_llist_get_first(&(pm->tests));
    test_obj = ((test_t*) amxc_container_of(test, test_t, it))->object;
    health = amxd_object_get_value(cstring_t, test_obj, "Health", NULL);
    assert_string_equal(health, "Good");

    amxc_var_clean(&ret_var);
    free(health);
}

void test_change_testinterval_test(UNUSED void** state) {
    amxd_dm_t* dm = get_dm();
    amxo_parser_t* parser = processmonitor_get_parser();
    amxb_bus_ctx_t* bus_ctx = amxut_bus_ctx();
    processmonitor_t* pm = pm_get_process_monitor();
    int rv = -1;
    amxc_var_t ret_var;
    amxc_var_t* pm_var = NULL;
    amxc_llist_it_t* test;
    amxd_object_t* test_obj = NULL;
    char* health = NULL;
    unsigned int test_interval = -1;
    unsigned int cur_test_interval = -1;
    unsigned int test_reset_interval = -1;
    amxd_trans_t trans;
    unsigned int cycle_duration = 0;
    unsigned int num_fail_actions = 0;
    unsigned int num_failed = 0;
    char* last_fail_reason = NULL;

    assert_non_null(dm);
    assert_non_null(parser);
    assert_non_null(bus_ctx);

    set_time(0);

    amxc_var_init(&ret_var);

    test = amxc_llist_get_first(&(pm->tests));
    test_obj = ((test_t*) amxc_container_of(test, test_t, it))->object;

    test_interval = amxd_object_get_value(uint32_t, test_obj, "TestInterval", NULL);
    cur_test_interval = amxd_object_get_value(uint32_t, test_obj, "CurrentTestInterval", NULL);
    num_failed = amxd_object_get_value(uint32_t, test_obj, "NumFailed", NULL);
    last_fail_reason = amxd_object_get_value(cstring_t, test_obj, "LastFailReason", NULL);
    assert_int_equal(test_interval, 3);
    assert_int_equal(cur_test_interval, 6);
    assert_int_equal(num_failed, 0);
    assert_string_equal(last_fail_reason, "Error_None");
    free(last_fail_reason);

    rv = amxb_get(bus_ctx, "ProcessMonitor.", 0, &ret_var, 3);
    assert_int_equal(rv, 0);
    pm_var = amxc_var_get_first(amxc_var_get_first(&ret_var));
    assert_non_null(pm_var);

    cycle_duration = GET_UINT32(pm_var, "CycleDuration");

    // change TestIntervalMultiplier
    // to increase the Test.CurrentTestInterval at each failed check
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "ProcessMonitor.Test.1");
    amxd_trans_set_value(cstring_t, &trans, "Subject", "IP");
    amxd_trans_set_value(uint32_t, &trans, "TestIntervalMultiplier", 2);
    amxd_trans_set_value(int32_t, &trans, "MaxFailNum", 5);
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);

    handle_events();

    set_add_time(cycle_duration);
    amxut_timer_go_to_future_ms(cycle_duration * 1000);

    health = amxd_object_get_value(cstring_t, test_obj, "Health", NULL);
    assert_string_equal(health, "Bad");

    for(int i = 1; i < 6; i++) {
        int future_interval = cycle_duration * pow(2, i);
        cur_test_interval = amxd_object_get_value(uint32_t, test_obj, "CurrentTestInterval", NULL);

        num_failed = amxd_object_get_value(uint32_t, test_obj, "NumFailed", NULL);
        assert_int_equal(num_failed, i);

        last_fail_reason = amxd_object_get_value(cstring_t, test_obj, "LastFailReason", NULL);
        assert_string_equal(last_fail_reason, "Error_None");
        free(last_fail_reason);

        assert_int_equal(cur_test_interval, future_interval);
        set_add_time(future_interval);
        amxut_timer_go_to_future_ms(future_interval * 1000);
    }

    num_fail_actions = amxd_object_get_value(uint32_t, test_obj, "NumFailActions", NULL);
    assert_int_equal(num_fail_actions, 1);

    last_fail_reason = amxd_object_get_value(cstring_t, test_obj, "LastFailReason", NULL);
    assert_string_equal(last_fail_reason, "Error_OccurencesTestFailed");
    free(last_fail_reason);

    // reset Test.CurrentTestInterval to Test.TestInterval (check succeed)
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "ProcessMonitor.Test.1");
    amxd_trans_set_value(cstring_t, &trans, "Subject", "VoiceService");
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);

    handle_events();

    cur_test_interval = amxd_object_get_value(uint32_t, test_obj, "CurrentTestInterval", NULL);
    set_add_time(cur_test_interval);
    amxut_timer_go_to_future_ms(cur_test_interval * 1000);

    test_reset_interval = amxd_object_get_value(uint32_t, test_obj, "TestResetInterval", NULL);
    set_add_time(test_reset_interval);
    amxut_timer_go_to_future_ms(test_reset_interval * 1000);

    cur_test_interval = amxd_object_get_value(uint32_t, test_obj, "CurrentTestInterval", NULL);
    assert_int_equal(cur_test_interval, cycle_duration);

    num_failed = amxd_object_get_value(uint32_t, test_obj, "NumFailed", NULL);
    assert_int_equal(num_failed, 0);

    amxc_var_clean(&ret_var);
    free(health);
}

void test_max_fail_duration(UNUSED void** state) {
    amxd_dm_t* dm = get_dm();
    amxo_parser_t* parser = processmonitor_get_parser();
    amxb_bus_ctx_t* bus_ctx = amxut_bus_ctx();
    processmonitor_t* pm = pm_get_process_monitor();
    int rv = -1;
    amxc_var_t ret_var;
    amxc_var_t* pm_var = NULL;
    amxc_llist_it_t* test;
    amxd_object_t* test_obj = NULL;
    char* health = NULL;
    unsigned int test_interval = -1;
    unsigned int cur_test_interval = -1;
    amxd_trans_t trans;
    unsigned int cycle_duration = 120;
    unsigned int num_fail_actions = 0;
    unsigned int max_fail_duration = 100;
    char* last_fail_reason = NULL;

    assert_non_null(dm);
    assert_non_null(parser);
    assert_non_null(bus_ctx);

    printf("Enter test_max_fail_duration\n");

    set_time(0);

    amxc_var_init(&ret_var);

    // trigger call to pm_write_cycle_duration
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "ProcessMonitor.");
    amxd_trans_set_value(uint32_t, &trans, "CycleDuration", cycle_duration);
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);

    test = amxc_llist_get_first(&(pm->tests));
    test_obj = ((test_t*) amxc_container_of(test, test_t, it))->object;

    test_interval = amxd_object_get_value(uint32_t, test_obj, "TestInterval", NULL);
    cur_test_interval = amxd_object_get_value(uint32_t, test_obj, "CurrentTestInterval", NULL);
    num_fail_actions = amxd_object_get_value(uint32_t, test_obj, "NumFailActions", NULL);
    last_fail_reason = amxd_object_get_value(cstring_t, test_obj, "LastFailReason", NULL);
    assert_int_equal(test_interval, 3);
    assert_int_equal(cur_test_interval, 6);
    assert_int_equal(num_fail_actions, 0);
    assert_string_equal(last_fail_reason, "Error_None");
    free(last_fail_reason);

    rv = amxb_get(bus_ctx, "ProcessMonitor.", 0, &ret_var, 3);
    assert_int_equal(rv, 0);
    pm_var = amxc_var_get_first(amxc_var_get_first(&ret_var));
    assert_non_null(pm_var);

    // change TestIntervalMultiplier
    // to increase the Test.CurrentTestInterval at each failed check
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "ProcessMonitor.Test.1");
    amxd_trans_set_value(cstring_t, &trans, "Subject", "IP");
    amxd_trans_set_value(uint32_t, &trans, "TestIntervalMultiplier", 5);
    amxd_trans_set_value(int32_t, &trans, "MaxFailNum", -1);
    amxd_trans_set_value(int32_t, &trans, "MaxFailDuration", max_fail_duration);
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);

    handle_events();

    set_add_time(cycle_duration);
    amxut_timer_go_to_future_ms(cycle_duration * 1000);

    health = amxd_object_get_value(cstring_t, test_obj, "Health", NULL);
    assert_string_equal(health, "Bad");

    cur_test_interval = amxd_object_get_value(uint32_t, test_obj, "CurrentTestInterval", NULL);
    assert_int_equal(cur_test_interval, 600);

    set_add_time(max_fail_duration);
    amxut_timer_go_to_future_ms(max_fail_duration * 1000);

    num_fail_actions = amxd_object_get_value(uint32_t, test_obj, "NumFailActions", NULL);
    assert_int_equal(num_fail_actions, 1);

    last_fail_reason = amxd_object_get_value(cstring_t, test_obj, "LastFailReason", NULL);
    assert_string_equal(last_fail_reason, "Error_DurationTestFailed");
    free(last_fail_reason);

    amxc_var_clean(&ret_var);
    free(health);
}

int test_teardown(void** state) {
    assert_int_equal(_processmonitor_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser()), 0);

    amxut_bus_teardown(state);
    return 0;
}
