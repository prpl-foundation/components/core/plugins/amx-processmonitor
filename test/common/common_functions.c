/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "../common/common_functions.h"
#include "dm_processmonitor.h"
#include "processmonitor_functions.h"
#include <amxut/amxut_bus.h>

static int time_sec = 0;

void handle_events(void) {
    amxut_bus_handle_events();
}

amxo_parser_t* test_setup_parser(void) {
    return amxut_bus_parser();
}

void test_setup_parse_odl(const char* odl_file_name) {
    amxd_object_t* root_obj = amxd_dm_get_root(amxut_bus_dm());
    if(0 != amxo_parser_parse_file(test_setup_parser(), odl_file_name, root_obj)) {
        fail_msg("PARSER MESSAGE = %s", amxc_string_get(&test_setup_parser()->msg, 0));
    }
}

// 'empty' replacements for functions in mod-dmext.so
static int _matches_regexp(void) {
    return AMXB_STATUS_OK;
}

void resolver_add_all_functions(void) {
    assert_int_equal(amxo_resolver_ftab_add(test_setup_parser(), "reset",
                                            AMXO_FUNC(_reset)), 0);
    assert_int_equal(amxo_resolver_ftab_add(test_setup_parser(), "pm_changed",
                                            AMXO_FUNC(_pm_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(test_setup_parser(), "pm_write_cycle_duration",
                                            AMXO_FUNC(_pm_write_cycle_duration)), 0);
    assert_int_equal(amxo_resolver_ftab_add(test_setup_parser(), "pm_read_test_interval",
                                            AMXO_FUNC(_pm_read_test_interval)), 0);
    assert_int_equal(amxo_resolver_ftab_add(test_setup_parser(), "pm_test_changed",
                                            AMXO_FUNC(_pm_test_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(test_setup_parser(), "pm_test_removed",
                                            AMXO_FUNC(_pm_test_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(test_setup_parser(), "pm_test_added",
                                            AMXO_FUNC(_pm_test_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(test_setup_parser(), "pm_delete_test",
                                            AMXO_FUNC(_pm_delete_test)), 0);
    assert_int_equal(amxo_resolver_ftab_add(test_setup_parser(), "matches_regexp",
                                            AMXO_FUNC(_matches_regexp)), 0);
}


// wrapped functions
int __wrap_stat(UNUSED const char* path, UNUSED struct stat* buf) {
    return 0;
}

int __wrap_system(UNUSED const char* cmd) {
    return 0;
}

void set_time(int current_time) {
    time_sec = current_time;
}

void set_add_time(int current_time) {
    time_sec += current_time;
}

time_t __wrap_time(time_t* t) {
    *t = time_sec;
    return *t;
}

int __wrap_amxc_ts_now(amxc_ts_t* tsp) {
    tsp->sec = time_sec;
    tsp->nsec = 0;
    tsp->offset = 0;
    return 0;
}
