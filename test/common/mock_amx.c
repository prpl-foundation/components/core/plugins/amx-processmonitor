/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "mock_amx.h"
#include "processmonitor.h"

#include <amxc/amxc_macros.h>

#include <setjmp.h>
#include <cmocka.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// wrapped functions
int __wrap_amxp_timer_new(amxp_timer_t** timer, amxp_timer_cb_t cb, void* priv) {
    assert_non_null(timer);
    assert_non_null(cb);
    assert_non_null(priv);

    *timer = (amxp_timer_t*) calloc(1, sizeof(amxp_timer_t));
    (*timer)->priv = priv;
    (*timer)->cb = cb;
    (*timer)->state = amxp_timer_off;

    return 0;
}
void __wrap_amxp_timer_delete(amxp_timer_t** timer) {
    assert_non_null(timer);
    free(*timer);
    *timer = NULL;
}

int __wrap_amxp_timer_start(amxp_timer_t* timer, UNUSED unsigned int timeout_msec) {
    assert_non_null(timer);
    timer->state = amxp_timer_started;
    return 0;
}

int __wrap_amxp_timer_stop(amxp_timer_t* timer) {
    assert_non_null(timer);
    timer->state = amxp_timer_off;
    return 0;
}

amxp_timer_state_t __wrap_amxp_timer_get_state(amxp_timer_t* timer) {
    return timer == NULL ? amxp_timer_off : timer->state;
}

unsigned int __wrap_amxp_timer_remaining_time(UNUSED amxp_timer_t* timer) {
    return 0;
}

int __wrap_amxp_subproc_new(UNUSED amxp_subproc_t** subproc) {
    return 0;
}

int __wrap_amxp_subproc_delete(UNUSED amxp_subproc_t** subproc) {
    return 0;
}

int __wrap_amxp_subproc_start(UNUSED amxp_subproc_t* const subproc, UNUSED char* cmd, ...) {
    return true;
}

bool __wrap_amxp_subproc_is_running(UNUSED const amxp_subproc_t* const subproc) {
    return false;
}

int __wrap_amxp_subproc_wait(UNUSED amxp_subproc_t* subproc, UNUSED int timeout_msec) {
    return 0;
}

int __wrap_amxp_subproc_kill(UNUSED const amxp_subproc_t* const subproc, UNUSED const int sig) {
    return 0;
}

int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx, const char* object, const char* method, amxc_var_t* args, amxc_var_t* ret, int timeout) {
    assert_non_null(bus_ctx);
    assert_non_null(object);
    assert_non_null(method);

    if(strcmp(object, "Device.") == 0) {
        check_expected(object);
        check_expected(method);
        assert_non_null(args);
        assert_non_null(ret);
        return 0;
    }

    return __real_amxb_call(bus_ctx, object, method, args, ret, timeout);
}

// mock functions
void mock_amxp_trigger_timer(amxp_timer_t* timer) {
    assert_false(timer->state == amxp_timer_off);
    timer->cb(timer, timer->priv);
}
