/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>

#include "common_functions.h"
#include "processmonitor_functions.h"
#include "test_check_test_types.h"

int test_setup(void** state) {
    amxut_bus_setup(state);
    resolver_add_all_functions();

    amxd_dm_t* dm = amxut_bus_dm();
    amxo_parser_t* parser = amxut_bus_parser();
    assert_non_null(dm);
    assert_non_null(parser);

    amxd_object_t* root = amxd_dm_get_root(dm);
    assert_non_null(root);

    test_setup_parse_odl(BASE_CONFIG);
    test_setup_parse_odl(TYPES_ODL_CONFIG);
    test_setup_parse_odl(MOCK_DEVICE_ODL);

    assert_int_equal(_processmonitor_main(AMXO_START, dm, parser), 0);
    handle_events();

    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:start", NULL);

    return 0;
}

void test_check_create_types(UNUSED void** state) {
    amxd_dm_t* dm = get_dm();
    amxo_parser_t* parser = processmonitor_get_parser();
    amxb_bus_ctx_t* bus_ctx = amxut_bus_ctx();
    processmonitor_t* pm = pm_get_process_monitor();
    int rv = -1;
    amxc_var_t ret_var;
    amxc_var_t* pm_var = NULL;
    unsigned int cycle_duration = 120;

    assert_non_null(dm);
    assert_non_null(parser);
    assert_non_null(bus_ctx);

    amxc_var_init(&ret_var);

    rv = amxb_get(bus_ctx, "ProcessMonitor.", 0, &ret_var, 3);
    assert_int_equal(rv, 0);
    pm_var = amxc_var_get_first(amxc_var_get_first(&ret_var));
    assert_non_null(pm_var);

    cycle_duration = GET_UINT32(pm_var, "CycleDuration");

    // check that the three tests have been correctly initialized
    amxc_llist_for_each(lit, &(pm->tests)) {
        test_t* temp_test = amxc_llist_it_get_data(lit, test_t, it);
        amxd_object_t* test_obj = NULL;
        char* name = NULL;
        char* type = NULL;
        test_obj = temp_test->object;
        assert_non_null(test_obj);

        name = amxd_object_get_value(cstring_t, test_obj, "Name", NULL);
        type = amxd_object_get_value(cstring_t, test_obj, "Type", NULL);

        if(strcmp(name, "voipapp") == 0) {
            assert_string_equal(type, "Plugin");
        }

        if(strcmp(name, "lucky") == 0) {
            assert_string_equal(type, "Process");
        }

        if(strcmp(name, "dummy") == 0) {
            assert_string_equal(type, "Custom");
        }

        free(name);
        free(type);
    }

    amxut_timer_go_to_future_ms(cycle_duration * 1000);
    amxc_var_clean(&ret_var);
}

void test_check_types_failaction(UNUSED void** state) {
    amxd_dm_t* dm = get_dm();
    amxo_parser_t* parser = processmonitor_get_parser();
    amxb_bus_ctx_t* bus_ctx = amxut_bus_ctx();
    processmonitor_t* pm = pm_get_process_monitor();
    int rv = -1;
    amxc_var_t ret_var;
    amxc_var_t* pm_var = NULL;
    amxd_trans_t trans;
    unsigned int cycle_duration = 120;
    char* last_fail_reason = NULL;
    amxd_object_t* test_obj_plugin = NULL;
    amxd_object_t* test_obj_process = NULL;
    amxd_object_t* test_obj_custom = NULL;

    assert_non_null(dm);
    assert_non_null(parser);
    assert_non_null(bus_ctx);

    amxc_var_init(&ret_var);

    rv = amxb_get(bus_ctx, "ProcessMonitor.", 0, &ret_var, 3);
    assert_int_equal(rv, 0);
    pm_var = amxc_var_get_first(amxc_var_get_first(&ret_var));
    assert_non_null(pm_var);

    cycle_duration = GET_UINT32(pm_var, "CycleDuration");

    // get objects for each test
    amxc_llist_for_each(lit, &(pm->tests)) {
        char* name = NULL;
        test_t* temp_test = amxc_llist_it_get_data(lit, test_t, it);
        name = amxd_object_get_value(cstring_t, temp_test->object, "Name", NULL);

        if(strcmp(name, "voipapp") == 0) {
            test_obj_plugin = temp_test->object;
        }

        if(strcmp(name, "lucky") == 0) {
            test_obj_process = temp_test->object;
        }

        if(strcmp(name, "dummy") == 0) {
            test_obj_custom = temp_test->object;
        }

        free(name);
    }

    assert_non_null(test_obj_plugin);
    assert_non_null(test_obj_process);
    assert_non_null(test_obj_custom);

    // force restart of timer_test
    pm->testing_active = false;

    // set tests in failing state
    amxd_trans_init(&trans);
    // plugin
    amxd_trans_select_pathf(&trans, "ProcessMonitor.Test.1");
    amxd_trans_set_value(cstring_t, &trans, "Subject", "IP");
    // process
    amxd_trans_select_pathf(&trans, "ProcessMonitor.Test.2");
    amxd_trans_set_value(cstring_t, &trans, "Subject", "5555");
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);

    handle_events();

    // custom
    set_custom_return(false);

    // loop on test execution and check fail values
    amxut_timer_go_to_future_ms(cycle_duration * 1000);

    assert_true(amxd_object_get_value(uint32_t, test_obj_plugin, "NumFailed", NULL) >= 1);
    assert_true(amxd_object_get_value(uint32_t, test_obj_process, "NumFailed", NULL) >= 1);
    assert_true(amxd_object_get_value(uint32_t, test_obj_custom, "NumFailed", NULL) >= 1);

    amxut_timer_go_to_future_ms(cycle_duration * 1000);

    assert_true(amxd_object_get_value(uint32_t, test_obj_plugin, "NumFailed", NULL) >= 2);
    assert_true(amxd_object_get_value(uint32_t, test_obj_process, "NumFailed", NULL) >= 2);
    assert_true(amxd_object_get_value(uint32_t, test_obj_custom, "NumFailed", NULL) >= 2);

    expect_string(__wrap_amxb_call, object, "Device.");
    expect_string(__wrap_amxb_call, method, "Reboot");
    amxut_timer_go_to_future_ms(cycle_duration * 1000);

    // Simulate reboot
    assert_int_equal(amxp_timer_stop(pm->reboot), 0);
    amxc_ts_parse(&pm->reboot_start, "2224-02-22T17:55:31Z", 20);
    pm->reboot_stage = PM_REBOOT_STAGE_NONE;

    // check failactions
    // (the fails on duration get triggered directly on health "Bad", time_t now stays the same)
    assert_true(amxd_object_get_value(uint32_t, test_obj_plugin, "NumFailActions", NULL) >= 1);
    assert_int_equal(amxd_object_get_value(uint32_t, test_obj_process, "NumFailActions", NULL), 0);
    assert_int_equal(amxd_object_get_value(uint32_t, test_obj_custom, "NumFailActions", NULL), 0);

    last_fail_reason = amxd_object_get_value(cstring_t, test_obj_plugin, "LastFailReason", NULL);
    assert_string_equal(last_fail_reason, "Error_OccurencesTestFailed");
    free(last_fail_reason);
    last_fail_reason = amxd_object_get_value(cstring_t, test_obj_process, "LastFailReason", NULL);
    assert_string_equal(last_fail_reason, "Error_None");
    free(last_fail_reason);
    last_fail_reason = amxd_object_get_value(cstring_t, test_obj_custom, "LastFailReason", NULL);
    assert_string_equal(last_fail_reason, "Error_None");
    free(last_fail_reason);

    // switch to default fail action for plugin
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "ProcessMonitor.Test.1");
    amxd_trans_set_value(cstring_t, &trans, "FailAction", "FAIL");
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);

    handle_events();

    amxut_timer_go_to_future_ms(cycle_duration * 1000);
    assert_true(amxd_object_get_value(uint32_t, test_obj_plugin, "NumFailActions", NULL) >= 2);

    last_fail_reason = amxd_object_get_value(cstring_t, test_obj_plugin, "LastFailReason", NULL);
    assert_string_equal(last_fail_reason, "Error_OccurencesTestFailed");
    free(last_fail_reason);

    amxc_var_clean(&ret_var);
}

void test_check_types_failaction_and_reboot(UNUSED void** state) {
    amxd_dm_t* dm = get_dm();
    amxo_parser_t* parser = processmonitor_get_parser();
    amxb_bus_ctx_t* bus_ctx = amxut_bus_ctx();
    processmonitor_t* pm = pm_get_process_monitor();
    int rv = -1;
    amxc_var_t ret;
    amxc_var_t ret_var;
    amxc_var_t* pm_var = NULL;
    amxd_trans_t trans;
    unsigned int cycle_duration = 120;
    amxd_object_t* test_obj_process = NULL;
    amxd_object_t* test_obj_custom = NULL;

    assert_non_null(dm);
    assert_non_null(parser);
    assert_non_null(bus_ctx);

    amxc_var_init(&ret);
    amxc_var_init(&ret_var);

    rv = amxb_get(bus_ctx, "ProcessMonitor.", 0, &ret_var, 3);
    assert_int_equal(rv, 0);
    pm_var = amxc_var_get_first(amxc_var_get_first(&ret_var));
    assert_non_null(pm_var);

    cycle_duration = GET_UINT32(pm_var, "CycleDuration");

    // get objects for each test
    amxc_llist_for_each(lit, &(pm->tests)) {
        char* name = NULL;
        test_t* temp_test = amxc_llist_it_get_data(lit, test_t, it);
        name = amxd_object_get_value(cstring_t, temp_test->object, "Name", NULL);

        if(strcmp(name, "lucky") == 0) {
            test_obj_process = temp_test->object;
        }

        if(strcmp(name, "dummy") == 0) {
            test_obj_custom = temp_test->object;
        }

        free(name);
    }

    assert_non_null(test_obj_process);
    assert_non_null(test_obj_custom);

    // force restart of timer_test
    pm->testing_active = false;

    assert_int_equal(amxb_call(bus_ctx, "ProcessMonitor.Test.2", "reset", NULL, &ret, 5), 0);
    assert_int_equal(amxb_call(bus_ctx, "ProcessMonitor.Test.3", "reset", NULL, &ret, 5), 0);

    // set tests in failing state
    amxd_trans_init(&trans);
    // process
    amxd_trans_select_pathf(&trans, "ProcessMonitor.Test.2");
    amxd_trans_set_value(cstring_t, &trans, "Subject", "4444");
    amxd_trans_set_value(uint32_t, &trans, "MaxFailNum", 2);
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);

    handle_events();

    // custom
    set_custom_return(false);

    // loop on test execution and check fail values
    amxut_timer_go_to_future_ms(cycle_duration * 1000);

    assert_true(amxd_object_get_value(uint32_t, test_obj_process, "NumFailed", NULL) >= 1);
    assert_true(amxd_object_get_value(uint32_t, test_obj_custom, "NumFailed", NULL) >= 1);

    amxut_timer_go_to_future_ms(cycle_duration * 1000);

    assert_true(amxd_object_get_value(uint32_t, test_obj_process, "NumFailed", NULL) >= 2);
    assert_true(amxd_object_get_value(uint32_t, test_obj_custom, "NumFailed", NULL) >= 2);

    amxut_timer_go_to_future_ms(cycle_duration * 1000);

    // check failactions
    // (the fails on duration get triggered directly on health "Bad", time_t now stays the same)
    assert_int_equal(amxd_object_get_value(uint32_t, test_obj_process, "NumFailActions", NULL), 1);
    assert_int_equal(amxd_object_get_value(uint32_t, test_obj_custom, "NumFailActions", NULL), 0);

    amxut_timer_go_to_future_ms(cycle_duration * 1000);
    assert_int_equal(amxd_object_get_value(uint32_t, test_obj_process, "NumFailActions", NULL), 2);

    expect_string(__wrap_amxb_call, object, "Device.");
    expect_string(__wrap_amxb_call, method, "Reboot");
    amxut_timer_go_to_future_ms(cycle_duration * 1000);

    // Simulate reboot
    assert_int_equal(amxp_timer_stop(pm->reboot), 0);
    amxc_ts_parse(&pm->reboot_start, "2224-02-22T17:55:31Z", 20);
    pm->reboot_stage = PM_REBOOT_STAGE_NONE;

    assert_int_equal(amxd_object_get_value(uint32_t, test_obj_process, "NumFailActions", NULL), 2);

    amxc_var_clean(&ret_var);
    amxc_var_clean(&ret);
}

void test_check_types_timeout(UNUSED void** state) {
    amxd_dm_t* dm = get_dm();
    amxo_parser_t* parser = processmonitor_get_parser();
    amxb_bus_ctx_t* bus_ctx = amxut_bus_ctx();
    processmonitor_t* pm = pm_get_process_monitor();
    int rv = -1;
    amxc_var_t ret_var;
    amxc_var_t* pm_var = NULL;
    amxd_trans_t trans;
    unsigned int cycle_duration = 120;
    unsigned int test_plugin_timeout = 1000;
    unsigned int test_custom_timeout = 1000;
    test_t* test_plugin = NULL;
    test_t* test_custom = NULL;
    amxd_object_t* test_obj_plugin = NULL;
    amxd_object_t* test_obj_custom = NULL;
    char* health_plug = NULL;
    char* health_custo = NULL;

    assert_non_null(dm);
    assert_non_null(parser);
    assert_non_null(bus_ctx);

    amxc_var_init(&ret_var);

    rv = amxb_get(bus_ctx, "ProcessMonitor.", 0, &ret_var, 3);
    assert_int_equal(rv, 0);
    pm_var = amxc_var_get_first(amxc_var_get_first(&ret_var));
    assert_non_null(pm_var);

    cycle_duration = GET_UINT32(pm_var, "CycleDuration");
    test_plugin_timeout = GET_UINT32(pm_var, "TestPluginTimeout");
    test_custom_timeout = GET_UINT32(pm_var, "TestCustomTimeout");

    assert_int_equal(test_plugin_timeout, 3000);
    assert_int_equal(test_custom_timeout, 5000);

    // get objects for tests with a timeout callback (plugin and custom)
    amxc_llist_for_each(lit, &(pm->tests)) {
        char* type = NULL;
        test_t* temp_test = amxc_llist_it_get_data(lit, test_t, it);
        type = amxd_object_get_value(cstring_t, temp_test->object, "Type", NULL);

        if(strcmp(type, "Plugin") == 0) {
            test_obj_plugin = temp_test->object;
        }

        if(strcmp(type, "Custom") == 0) {
            test_obj_custom = temp_test->object;
        }

        free(type);
    }

    test_plugin = test_obj_plugin->priv;
    test_custom = test_obj_custom->priv;
    assert_non_null(test_obj_plugin);
    assert_non_null(test_plugin);
    assert_non_null(test_obj_custom);
    assert_non_null(test_custom);

    // force restart of timer_test
    pm->testing_active = false;

    // set tests in good state
    amxd_trans_init(&trans);
    // plugin
    amxd_trans_select_pathf(&trans, "ProcessMonitor.Test.1");
    amxd_trans_set_value(cstring_t, &trans, "Subject", "VoiceService");
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);

    handle_events();

    // custom
    set_custom_return(true);

    amxut_timer_go_to_future_ms(cycle_duration * 1000);

    // force timeout and tests active to change health
    assert_true(test_plugin->funcs->timed_out);
    assert_true(test_custom->funcs->timed_out);
    test_plugin->req = NULL;
    test_plugin->active = true;
    test_plugin->funcs->timed_out(test_plugin);

    test_custom->active = true;
    test_custom->funcs->timed_out(test_custom);

    health_plug = amxd_object_get_value(cstring_t, test_obj_plugin, "Health", NULL);
    health_custo = amxd_object_get_value(cstring_t, test_obj_custom, "Health", NULL);
    assert_string_equal(health_plug, "Bad");
    assert_string_equal(health_custo, "Bad");
    free(health_plug);
    free(health_custo);

    amxc_var_clean(&ret_var);
}

int test_teardown(void** state) {
    assert_int_equal(_processmonitor_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser()), 0);

    amxut_bus_teardown(state);
    return 0;
}
