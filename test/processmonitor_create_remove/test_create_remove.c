/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>

#include "processmonitor_functions.h"
#include "common_functions.h"
#include "test_create_remove.h"

int test_setup(void** state) {
    amxut_bus_setup(state);
    resolver_add_all_functions();

    amxd_dm_t* dm = amxut_bus_dm();
    amxo_parser_t* parser = amxut_bus_parser();
    assert_non_null(dm);
    assert_non_null(parser);

    amxd_object_t* root = amxd_dm_get_root(dm);
    assert_non_null(root);

    test_setup_parse_odl(BASE_CONFIG);
    test_setup_parse_odl(BAD_ODL_CONFIG);
    test_setup_parse_odl(MOCK_DEVICE_ODL);

    assert_int_equal(_processmonitor_main(AMXO_START, dm, parser), 0);
    handle_events();

    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:start", NULL);

    return 0;
}

void test_creation_test(UNUSED void** state) {
    amxd_dm_t* dm = get_dm();
    amxo_parser_t* parser = processmonitor_get_parser();
    amxb_bus_ctx_t* bus_ctx = amxut_bus_ctx();
    int rv = -1;
    amxc_var_t ret_var_1;
    amxc_var_t* test_var_1 = NULL;
    amxc_var_t ret_var_2;
    amxc_var_t* test_var_2 = NULL;

    assert_non_null(dm);
    assert_non_null(parser);
    assert_non_null(bus_ctx);

    amxc_var_init(&ret_var_1);

    rv = amxb_get(bus_ctx, "ProcessMonitor.Test.1.", 0, &ret_var_1, 3);
    assert_int_equal(rv, 0);
    test_var_1 = amxc_var_get_first(amxc_var_get_first(&ret_var_1));
    assert_non_null(test_var_1);

    assert_string_equal(GET_CHAR(test_var_1, "Name"), "voipapp");
    assert_string_equal(GET_CHAR(test_var_1, "Type"), "Plugin");
    assert_string_equal(GET_CHAR(test_var_1, "Subject"), "VoiceService");
    assert_int_equal(GET_INT32(test_var_1, "MaxFailNum"), 2);
    assert_int_equal(GET_INT32(test_var_1, "MaxFailDuration"), -1);
    assert_string_equal(GET_CHAR(test_var_1, "FailAction"), "NO_ACTION");
    assert_int_equal(GET_UINT32(test_var_1, "TestInterval"), 3);
    assert_int_equal(GET_INT32(test_var_1, "TestResetInterval"), 3000);

    amxc_var_clean(&ret_var_1);

    rv = -1;

    amxc_var_init(&ret_var_2);

    rv = amxb_get(bus_ctx, "ProcessMonitor.Test.2.", 0, &ret_var_2, 3);
    assert_int_equal(rv, 0);
    test_var_2 = amxc_var_get_first(amxc_var_get_first(&ret_var_2));
    assert_non_null(test_var_2);

    assert_string_equal(GET_CHAR(test_var_2, "Name"), "wrong");
    assert_string_equal(GET_CHAR(test_var_2, "Type"), "Plugin");
    assert_string_equal(GET_CHAR(test_var_2, "Subject"), "");
    assert_int_equal(GET_INT32(test_var_2, "MaxFailNum"), -1);
    assert_int_equal(GET_INT32(test_var_2, "MaxFailDuration"), -1);
    assert_string_equal(GET_CHAR(test_var_2, "FailAction"), "REBOOT");
    assert_int_equal(GET_UINT32(test_var_2, "TestInterval"), 3);
    assert_int_equal(GET_INT32(test_var_2, "TestResetInterval"), 3000);

    amxc_var_clean(&ret_var_2);
}

void test_reset_test(UNUSED void** state) {
    amxd_dm_t* dm = get_dm();
    amxo_parser_t* parser = processmonitor_get_parser();
    amxb_bus_ctx_t* bus_ctx = amxut_bus_ctx();
    int rv = -1;
    amxc_var_t ret_var;
    amxc_var_t* test_var = NULL;
    amxc_var_t ret;
    amxc_ts_t ref_ts = {0, 0, 0};
    const amxc_ts_t* cur_ts = NULL;

    assert_non_null(dm);
    assert_non_null(parser);
    assert_non_null(bus_ctx);

    amxc_var_init(&ret_var);
    amxc_var_init(&ret);

    assert_int_equal(amxb_call(bus_ctx, "ProcessMonitor.Test.1", "reset", NULL, &ret, 5), 0);

    rv = amxb_get(bus_ctx, "ProcessMonitor.Test.1.", 0, &ret_var, 3);
    assert_int_equal(rv, 0);
    test_var = amxc_var_get_first(amxc_var_get_first(&ret_var));
    assert_non_null(test_var);

    assert_int_equal(GET_UINT32(test_var, "NumFailed"), 0);
    assert_int_equal(GET_UINT32(test_var, "NumFailActions"), 0);
    assert_string_equal(GET_CHAR(test_var, "LastFailReason"), "Error_None");

    cur_ts = amxc_var_constcast(amxc_ts_t, GET_ARG(test_var, "LastCheck"));

    assert_true(amxc_ts_is_valid(cur_ts));
    assert_true(ref_ts.sec == cur_ts->sec);
    assert_true(ref_ts.nsec == cur_ts->nsec);

    cur_ts = amxc_var_constcast(amxc_ts_t, GET_ARG(test_var, "LastSuccess"));

    assert_true(amxc_ts_is_valid(cur_ts));
    assert_true(ref_ts.sec == cur_ts->sec);
    assert_true(ref_ts.nsec == cur_ts->nsec);

    cur_ts = amxc_var_constcast(amxc_ts_t, GET_ARG(test_var, "FailedSince"));

    assert_true(amxc_ts_is_valid(cur_ts));
    assert_true(ref_ts.sec == cur_ts->sec);
    assert_true(ref_ts.nsec == cur_ts->nsec);

    cur_ts = amxc_var_constcast(amxc_ts_t, GET_ARG(test_var, "LastFailAction"));

    assert_true(amxc_ts_is_valid(cur_ts));
    assert_true(ref_ts.sec == cur_ts->sec);
    assert_true(ref_ts.nsec == cur_ts->nsec);

    amxc_var_clean(&ret_var);
    amxc_var_clean(&ret);
}

void test_removal_test(UNUSED void** state) {
    amxd_dm_t* dm = get_dm();
    amxo_parser_t* parser = processmonitor_get_parser();
    amxb_bus_ctx_t* bus_ctx = amxut_bus_ctx();
    int rv = -1;
    amxc_var_t ret_var;
    amxc_var_t ret;

    assert_non_null(dm);
    assert_non_null(parser);
    assert_non_null(bus_ctx);

    amxc_var_init(&ret_var);
    amxc_var_init(&ret);

    assert_int_equal(amxb_del(bus_ctx, "ProcessMonitor.Test.1.", 1, NULL, &ret, 5), 0);
    handle_events();
    amxut_timer_go_to_future_ms(10000);

    rv = amxb_get(bus_ctx, "ProcessMonitor.Test.1.", 0, &ret_var, 3);
    assert_int_not_equal(rv, 0);

    amxc_var_clean(&ret_var);
    amxc_var_clean(&ret);
}

int test_teardown(void** state) {
    assert_int_equal(_processmonitor_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser()), 0);

    amxut_bus_teardown(state);
    return 0;
}
