# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.2.8 - 2024-10-03(08:34:19 +0000)

### Other

- - CPE reboot after n retry

## Release v0.2.7 - 2024-10-02(18:47:35 +0000)

### Other

- - Failure Action reason

## Release v0.2.6 - 2024-09-10(07:17:47 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v0.2.5 - 2024-08-27(13:25:30 +0000)

### Other

- - [ProcessMonitor] CurrentTestInterval is increased incorrect

## Release v0.2.4 - 2024-07-20(06:10:46 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v0.2.3 - 2024-07-10(15:23:44 +0000)

### Fixes

- [ProcessMonitor] Use CurrentTestInterval when greater than CycleDuration

## Release v0.2.2 - 2024-05-16(13:40:52 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.2.1 - 2024-04-10(07:18:00 +0000)

### Fixes

- [amx-processmonitor] Crash when removing a test instance

## Release v0.2.0 - 2024-03-20(08:59:22 +0000)

### New

- Set correct reboot reason in case of reboot due to ProcessMonitor

## Release v0.1.9 - 2024-03-14(18:54:40 +0000)

### Fixes

- Wrong trace level

## Release v0.1.8 - 2024-01-23(20:29:47 +0000)

### Fixes

- Do not restart the firewall in case it stops working

## Release v0.1.7 - 2024-01-22(15:04:41 +0000)

### Other

- create instances by name iso index

## Release v0.1.6 - 2023-12-19(11:23:50 +0000)

### Fixes

- [ProcessMonitor] cleanup plugin

## Release v0.1.5 - 2023-12-11(12:15:51 +0000)

### Fixes

- Implementation of debugInfo is Missing.

## Release v0.1.4 - 2023-12-07(10:41:23 +0000)

### Fixes

- add object-changed events for automated tests

## Release v0.1.3 - 2023-12-05(20:22:49 +0000)

### Changes

- Remove deviceid as default and replace by firewall

## Release v0.1.2 - 2023-11-23(16:32:21 +0000)

### Fixes

- ProcessMonitor initialisation

## Release v0.1.1 - 2023-11-10(11:37:05 +0000)

### Fixes

- [processmonitor] Hardcode CONFIG_RWDATAPATH globally

### Other

- Opensource component

## Release v0.1.0 - 2023-10-23(13:49:29 +0000)

### New

- [ProcessMonitor] implementation of the processmonitor

