%define {
    %persistent object ProcessMonitor {
        on event "dm:object-changed" call pm_changed;
        /**
         * The duration between the execution of the same test in seconds.
         * @version 1.0
         */
        %persistent uint32 CycleDuration {
            on action write call pm_write_cycle_duration;
            default 120;
        }
        /**
         * Time (ms) before considering a Plugin Test as failed
         */
        %persistent uint32 TestPluginTimeout {
            default 3000;
        }
        /**
         * Time (ms) before considering a Custom Test as failed
         */
        %persistent uint32 TestCustomTimeout {
            default 5000;
        }
        /**
         * The interval between 2 consecutive tests in seconds.
         * @version 1.0
         */
        %read-only uint32 TestInterval {
            on action read call pm_read_test_interval;
        }
        /**
         * The maximum number of reboots a test can enforce.
         * @version 1.0
         */
        %persistent uint32 MaxReboots = 3;
        /**
         * Timestamp when the last reboot occurred (in UTC).
         * @version 1.0
         */
        %persistent %read-only datetime LastReboot;
        /**
         * Reason of the last reboot.
         * @version 1.0
         */
        %persistent %read-only string RebootReason = "Unknown";
        /**
         * Configuration of the different tests.
         * @version 1.0
         */
        %persistent object Test[] {
            counted with NumberOfTest;
            on event "dm:instance-added" call pm_test_added;
            on event "dm:instance-removed" call pm_test_removed;
            on event "dm:object-changed" call pm_test_changed;
            on action del-inst call pm_delete_test;
            /**
             * The name of the test
             * @version 1.0
             */
            %persistent %read-only string Name;
            /**
             * Health of the test.
             * - Good: the test succeeded
             * - Bad: the test failed
             * - Awaiting check: the test has not been checked yet since start or reconfiguration
             * - Bad configuration: the configuration is not valid
             * @version 1.0
             */
            %read-only string Health {
                on action validate call check_enum ["Good", "Bad", "Awaiting check", "Bad configuration"];
                default "Bad configuration";
            }
            /**
             * The type of test.
             * @version 1.0
             */
            %persistent string Type {
                on action validate call check_enum ["Plugin", "Process", "Custom"];
                default "Plugin";
            }
            /**
             * The subject of the test, depending on the Type.
             * - Plugin: Datamodel path
             * - Process: PID
             * - Custom: ignored. The script '/usr/lib/processmonitor/scripts/<Name> test'
             * will be executed to test. The test will be considered successful if the return value of the
             * script is 0
             * @version 1.0
             */
            %persistent string Subject = "";
            /**
             * Amount of times the test fails before a reboot is triggered.
             * When set to 0 no reboot is triggered.
             * @version 1.0
             */
            %persistent uint32 RebootAfterRestartThreshold = 0;
            /**
             * Maximum number of allowed failed results before action is undertaken.
             * If set to -1 then this is ignored.
             * @version 1.0
             */
            %persistent int32 MaxFailNum = -1;
            /**
             * Number of seconds of failed results after which action is undertaken.
             * If set to -1 then this is ignored.
             * @version 1.0
             */
            %persistent int32 MaxFailDuration = -1;
            /**
             * The action to undertake.
             * - keyword 'REBOOT': Perform a reboot
             * - keyword 'NO_ACTION': No action will be performed. The NumFailed counter will increase.
             * - keyword 'RESTART': The script '/etc/init.d/<Name> restart' will be executed.
             * - other: ignored. The script '/usr/lib/processmonitor/scripts/<Name> fail' will be executed.
             * @version 1.0
             */
            %persistent string FailAction = "REBOOT";
            /**
             * Last check performed upon this test (in UTC).
             * @version 1.0
             */
            %read-only datetime LastCheck;
            /**
             * Last successful check of this test (in UTC).
             * @version 1.0
             */
            %read-only datetime LastSuccess;
            /**
             * Timestamp when test started to fail (in UTC).
             * @version 1.0
             */
            %read-only datetime FailedSince;
            /**
             * Timestamp when test started to succeed (in UTC).
             * @version 1.0
             */
            %read-only datetime SuccessfulSince;
            /**
             * Number of failed checks.
             * @version 1.0
             */
            %read-only uint32 NumFailed = 0;
            /**
             * Timestamp of when the last FailAction of the test was executed (in UTC).
             * @version 1.0
             */
            %persistent %read-only datetime LastFailAction;
            /**
             * Reason why the last FailAction of the test was executed.
             * - Error_DurationTestFailed:   Test failed for longer than MaxFailDuration seconds
             * - Error_OccurencesTestFailed: Test failed more than MaxFailNum times
             * - Error_None: Test passed
             * @version 1.0
             */
            %persistent %read-only string LastFailReason {
                on action validate call check_enum ["Error_None", "Error_DurationTestFailed", "Error_OccurencesTestFailed"];
                default "Error_None";
            }
            /**
             * Number of times that the FailAction was executed.
             * @version 1.0
             */
            %persistent %read-only uint32 NumFailActions = 0;
            /**
             * Reset timestamps and counters for this test.
             * @version 1.0
             */
            void reset();
            /**
            * The minimum time in seconds between each test.
            * @version 1.0
            */
            %persistent uint32 TestInterval = 0;
            /**
            * Amount by which the TestInterval will be multiplied after
            * each failed test. The TestInterval will however never be greater than MaxFailDuration
            * if this is specified.
            * This should be greater than or equal to 1.
            * @version 1.0
            */
            %persistent uint32 TestIntervalMultiplier
            {
                on action validate call check_minimum 1;
                default 1;
            }
            /**
            * The currenly used test interval in seconds.
            * @version 1.0
            */
            %read-only uint32 CurrentTestInterval;
            /**
            * The time in seconds how long a test should be successful before the CurrentTestInterval
            * is reset to its default value.
            * @version 1.0
            */
            %persistent int32 TestResetInterval = 3600;
        }
    }
}
