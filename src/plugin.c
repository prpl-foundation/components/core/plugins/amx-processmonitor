/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "processmonitor.h"

static bool pm_plugin_stop(test_t* test);

static void pm_plugin_test_done_handler(UNUSED const amxb_bus_ctx_t* bus_ctx,
                                        amxb_request_t* request,
                                        int status,
                                        void* priv) {
    test_t* test = priv;

    SAH_TRACEZ_INFO(ME, "Plugin test status %d", status);
    when_null_trace(test, exit, ERROR, "Plugin test is NULL");

    amxp_timer_stop(test->timer_check);
    amxb_close_request(&(test->req));
    test->req = NULL;

    test->has_result = true;

    if((request != NULL) && (status == 0)) {
        SAH_TRACEZ_INFO(ME, "Plugin test '%s' verified to be OK", test->name);
        test->result = true;
    } else {
        SAH_TRACEZ_WARNING(ME, "Plugin test '%s' failed verification with error code %d", test->name ? test->name : "", status);
        test->result = false;
    }

    pm_handle_result(test);

exit:
    return;
}

static bool pm_plugin_provide(test_t* test) {
    SAH_TRACEZ_INFO(ME, "Providing environment for plugin test '%s'", test->name);

    if(test->req != NULL) {
        SAH_TRACEZ_ERROR(ME, "Programming error - providing plugin test while something is already provided");
        return false;
    }

    return true;
}

static void pm_plugin_release(test_t* test) {
    SAH_TRACEZ_INFO(ME, "Releasing environment for plugin test '%s'", test->name);

    pm_plugin_stop(test);
}

static bool pm_plugin_perform(test_t* test) {
    amxb_bus_ctx_t* bus_ctx = NULL;
    SAH_TRACEZ_NOTICE(ME, "Verifying plugin test '%s', path is '%s'", test->name, test->subject);

    if(test->req != NULL) {
        SAH_TRACEZ_ERROR(ME, "Environment is not ok to perform test");
        return false;
    }

    amxp_timer_start(test->timer_check, amxd_object_get_value(uint32_t, test->pm->object, "TestPluginTimeout", NULL));
    bus_ctx = amxb_be_who_has(test->subject);
    if(bus_ctx == NULL) {
        SAH_TRACEZ_ERROR(ME, "No bus ctx for plugin test '%s'", test->name);
        return false;
    }

    test->req = amxb_async_call(bus_ctx, test->subject, "_get", NULL, pm_plugin_test_done_handler, test);

    if(test->req == NULL) {
        SAH_TRACEZ_WARNING(ME, "Failed to verify plugin test '%s'", test->name);
        return false;
    }

    return true;
}

static bool pm_plugin_stop(test_t* test) {
    SAH_TRACEZ_NOTICE(ME, "Stop verification of plugin test '%s'", test->name);

    amxp_timer_stop(test->timer_check);
    amxb_close_request(&(test->req));
    test->req = NULL;

    return true;
}

static void pm_plugin_timed_out(test_t* test) {
    amxb_close_request(&(test->req));
    test->req = NULL;

    SAH_TRACEZ_WARNING(ME, "Plugin test '%s' timed out on verification", test->name);
    test->has_result = true;
    test->result = false;

    pm_handle_result(test);
}

static test_functions_t plugin_funcs =
{
    .provide = pm_plugin_provide,
    .release = pm_plugin_release,
    .perform = pm_plugin_perform,
    .stop = pm_plugin_stop,
    .timed_out = pm_plugin_timed_out,
};

void pm_set_plugin_functions(test_t* test) {
    test->funcs = &plugin_funcs;
}
