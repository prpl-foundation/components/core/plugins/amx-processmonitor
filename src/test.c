/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <sys/stat.h>
#include <math.h>

#include "processmonitor.h"

#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif

#define STR_EMPTY(_x) ((_x) == NULL || (_x)[0] == '\0')

static test_functions_t empty_funcs;

static void update_current_test_interval(test_t* test) {
    unsigned int default_test_interval = 0;

    when_null(test, exit);
    // Update current_test_interval only if the test isn't in recovery mode
    when_true(test->recovery, exit);

    default_test_interval = test->pm->test_interval * test->pm->num_valid_tests;
    test->current_test_interval = MAX(test->test_interval, default_test_interval) * pow(test->test_interval_multiplier, test->num_failed);

exit:
    return;
}

static int pm_str2type(const char* type) {
    if(type == NULL) {
        return PM_TEST_TYPE_INVALID;
    } else if(!strcmp(type, "Plugin")) {
        return PM_TEST_TYPE_PLUGIN;
    } else if(!strcmp(type, "Process")) {
        return PM_TEST_TYPE_PROCESS;
    } else if(!strcmp(type, "Custom")) {
        return PM_TEST_TYPE_CUSTOM;
    } else {
        return PM_TEST_TYPE_INVALID;
    }
}

static bool pm_update_int32_value(int* current, int new) {
    if(*current == new) {
        return false;
    }

    *current = new;
    return true;
}

static bool pm_update_uint32_value(unsigned int* current, unsigned int new) {
    if(*current == new) {
        return false;
    }

    *current = new;
    return true;
}

static bool pm_update_char_value(char** current, const char* new) {
    const char* old;

    old = (const char*) *current;

    if(!old && !new) {
        return false;
    } else if(old && new && !strcmp(old, new)) {
        return false;
    }

    free(*current);
    *current = NULL;

    if(new) {
        *current = strdup(new);
    }

    return true;
}

static void pm_set_fail_actions_param(amxd_object_t* object, uint32_t cnt, amxc_ts_t ts_now) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(uint32_t, &trans, "NumFailActions", cnt);
    amxd_trans_set_value(amxc_ts_t, &trans, "LastFailAction", &ts_now);
    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }
    amxd_trans_clean(&trans);
}

static void pm_execute_action_reboot(test_t* test, bool too_often, amxc_ts_t ts_now) {
    uint32_t max = 0;
    uint32_t num = 0;
    amxc_string_t reason;

    amxc_string_init(&reason, 0);

    when_null(test, exit);
    num = amxd_object_get_value(uint32_t, test->object, "NumFailActions", NULL);
    max = amxd_object_get_value(uint32_t, test->pm->object, "MaxReboots", NULL);
    when_true_trace(num >= max, exit, ERROR, "The test failure should trigger a reboot, but this test has caused too many reboots! (%u >= %u)", num, max);

    SAH_TRACEZ_WARNING(ME, "Executing reboot");
    amxc_string_setf(&reason, "Test '%s' failed %s", test->name, too_often ? "too often" : "for too long");

    pm_set_fail_actions_param(test->object, num + 1, ts_now);
    pm_perform_reboot(test->pm, amxc_string_get(&reason, 0), ts_now);

exit:
    amxc_string_clean(&reason);
}

static void pm_execute_action_no_action(test_t* test, amxc_ts_t ts_now) {
    uint32_t num = 0;

    when_null(test, exit);
    num = amxd_object_get_value(uint32_t, test->object, "NumFailActions", NULL);
    SAH_TRACEZ_WARNING(ME, "%s: " PM_FAIL_ACTION_NO_ACTION, test->name);
    pm_set_fail_actions_param(test->object, num + 1, ts_now);
    test->fail_action_executed = true;
    // dont trigger a save as a hack for sysbus monitoring
    // assume reboot if sysbus dies

exit:
    return;
}

static void pm_execute_action_restart(test_t* test, amxc_ts_t ts_now) {
    uint32_t num = 0;
    amxc_string_t buffer;

    amxc_string_init(&buffer, 0);

    when_null(test, exit);
    when_true_trace(amxp_subproc_is_running(test->fail_process), exit, WARNING, "Should execute action " PM_FAIL_ACTION_RESTART " but the previous iteration is still running");

    num = amxd_object_get_value(uint32_t, test->object, "NumFailActions", NULL);

    if((test->reboot_after_restart_threshold > 0) && (num >= test->reboot_after_restart_threshold)) {
        SAH_TRACEZ_WARNING(ME, "Executing reboot");
        amxc_string_setf(&buffer, "Test '%s' failed too often", test->name);

        pm_perform_reboot(test->pm, amxc_string_get(&buffer, 0), ts_now);
    } else {
        amxc_string_setf(&buffer, "/etc/init.d/%s restart", test->name);

        SAH_TRACEZ_WARNING(ME, "Executing fail action: '%s'", amxc_string_get(&buffer, 0));
        pm_set_fail_actions_param(test->object, num + 1, ts_now);
        amxp_subproc_start(test->fail_process, (char*) "/bin/sh", "-c", amxc_string_get(&buffer, 0), NULL);
    }

    test->fail_action_executed = true;

exit:
    amxc_string_clean(&buffer);
}

static void pm_execute_action_other(test_t* test, amxc_ts_t ts_now, UNUSED const char* action) {
    uint32_t num = 0;
    amxc_string_t cmd;

    amxc_string_init(&cmd, 0);

    when_null(test, exit);
    when_true_trace(amxp_subproc_is_running(test->fail_process), exit, WARNING, "Should execute action '%s' but the previous iteration is still running", action);

    num = amxd_object_get_value(uint32_t, test->object, "NumFailActions", NULL);
    amxc_string_setf(&cmd, "%s%s fail", SCRIPT_DIR, test->name);

    SAH_TRACEZ_WARNING(ME, "Executing fail action: '%s'", amxc_string_get(&cmd, 0));
    pm_set_fail_actions_param(test->object, num + 1, ts_now);
    amxp_subproc_start(test->fail_process, (char*) "/bin/sh", "-c", amxc_string_get(&cmd, 0), NULL);
    test->fail_action_executed = true;

exit:
    amxc_string_clean(&cmd);
}

static void pm_execute_action(test_t* test, bool too_often) {
    const char* action = NULL;
    amxc_ts_t ts_now = {0, 0, 0};

    when_null(test, exit);
    action = amxc_var_constcast(cstring_t, amxd_object_get_param_value(test->object, "FailAction"));
    when_str_empty_trace(action, exit, WARNING, "No action defined - should not happen!");

    amxc_ts_now(&ts_now);

    if(strcmp(action, PM_FAIL_ACTION_REBOOT) == 0) {
        pm_execute_action_reboot(test, too_often, ts_now);
    } else if(strcmp(action, PM_FAIL_ACTION_NO_ACTION) == 0) {
        pm_execute_action_no_action(test, ts_now);
    } else if(strcmp(action, PM_FAIL_ACTION_RESTART) == 0) {
        pm_execute_action_restart(test, ts_now);
    } else {
        pm_execute_action_other(test, ts_now, action);
    }

exit:
    return;
}

static const char* pm_check_test(test_t* test) {
    int32_t max_fail_num = 0;
    int32_t max_fail_duration = 0;
    const char* fail_reason = NULL;

    when_null_trace(test, exit, WARNING, "Test is NULL");
    when_false(test->valid_conf, exit);

    SAH_TRACEZ_INFO(ME, "Checking test '%s'", test->name);

    max_fail_num = amxd_object_get_value(int32_t, test->object, "MaxFailNum", NULL);
    max_fail_duration = amxd_object_get_value(int32_t, test->object, "MaxFailDuration", NULL);

    if(max_fail_num >= 0) {
        if((int) amxd_object_get_value(uint32_t, test->object, "NumFailed", NULL) >= max_fail_num) {
            SAH_TRACEZ_WARNING(ME, "Test '%s' failed too often, executing action", test->name);
            pm_execute_action(test, true);
            fail_reason = PM_TEST_FAIL_REASON_TEST_OCCURENCES;
            goto exit;
        }
    }

    if(max_fail_duration >= 0) {
        time_t now;
        time_t failed_since;
        struct tm tm_failed_since;

        time(&now);
        amxc_ts_to_tm_utc(&test->failed_since, &tm_failed_since);
        failed_since = timegm(&tm_failed_since);
        if(difftime(now, failed_since) >= test->max_fail_duration) {
            SAH_TRACEZ_WARNING(ME, "Test '%s' failed for too long, executing action", test->name);
            pm_execute_action(test, false);
            fail_reason = PM_TEST_FAIL_REASON_TEST_TIMEOUT;
        }
    }
exit:
    return fail_reason;
}

static void pm_test_timed_out(UNUSED amxp_timer_t* timer, void* userdata) {
    test_t* test = (test_t*) userdata;

    when_null_trace(test, exit, WARNING, "Test timed out but no userdata provided");
    when_null_trace(test->funcs->timed_out, exit, WARNING, "No timeout function");

    test->funcs->timed_out(test);

exit:
    return;
}

static bool pm_check_config_validity(test_t* test, bool script_exists, char* script) {
    bool valid_conf = false;
    (void) script;

    when_true_trace(test->type == PM_TEST_TYPE_INVALID, exit, ERROR, "The type is invalid");

    if(test->type != PM_TEST_TYPE_CUSTOM) {
        when_true_trace((test->subject == NULL) || (test->subject[0] == '\0'), exit, ERROR, "The Subject is empty");
    } else {
        when_false_trace(script_exists, exit, ERROR, "The script '%s' does not exist", script);
    }

    when_true_trace(((test->fail_action == NULL) ||
                     ((strcmp(test->fail_action, PM_FAIL_ACTION_REBOOT) != 0) &&
                      (strcmp(test->fail_action, PM_FAIL_ACTION_NO_ACTION) != 0) &&
                      (strcmp(test->fail_action, PM_FAIL_ACTION_RESTART) != 0)))
                    && !script_exists, exit, ERROR, "The script '%s' does not exist", script);
    when_true_trace((test->funcs->validate != NULL) && !test->funcs->validate(test), exit, ERROR, "The validate function is not defined");
    valid_conf = true;
exit:
    test->valid_conf = valid_conf;

    return valid_conf;
}

static void pm_validate_test(test_t* test) {
    bool script_exists = false;
    struct stat s;
    char script[255] = {0};
    amxd_trans_t trans;

    when_null_trace(test, exit, WARNING, "Test is NULL");

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, test->object);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    snprintf(script, 255, "%s%s", SCRIPT_DIR, test->name);
    script_exists = !stat(script, &s);

    if(pm_check_config_validity(test, script_exists, script) == false) {
        amxd_trans_set_value(cstring_t, &trans, "Health", "Bad configuration");
    } else {
        amxd_trans_set_value(cstring_t, &trans, "Health", "Awaiting check");
    }

    update_current_test_interval(test);

    amxd_trans_set_value(uint32_t, &trans, "CurrentTestInterval", test->current_test_interval);

    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }

    pm_handle_test_update(test->pm, test);
    amxd_trans_clean(&trans);

exit:
    return;
}

static void pm_validate_tests(amxc_llist_t* tests) {
    amxc_llist_for_each(lit, tests) {
        test_t* temp_test = amxc_llist_it_get_data(lit, test_t, it);
        pm_validate_test(temp_test);
    }
}

static test_t* pm_create_test(processmonitor_t* pm, amxd_object_t* object) {
    test_t* test = calloc(1, sizeof(test_t));

    when_null_trace(test, exit, WARNING, "Test is NULL, Unable to create Test data structure");

    if(amxp_subproc_new(&(test->fail_process)) != 0) {
        SAH_TRACEZ_ERROR(ME, "Unable to create Process information for FailAction");
        free(test);
        test = NULL;
        goto exit;
    }

    if(amxp_timer_new(&(test->timer_check), pm_test_timed_out, test) != 0) {
        SAH_TRACEZ_ERROR(ME, "Unable to create Test Check timer");
        amxp_subproc_delete(&(test->fail_process));
        free(test);
        test = NULL;
        goto exit;
    }

    test->name = amxd_object_get_value(cstring_t, object, "Name", NULL);
    test->recovery = false;
    test->pm = pm;
    test->object = object;
    object->priv = test;
    amxc_llist_it_init(&(test->it));
    amxc_llist_append(&(pm->tests), &(test->it));
    test->type = PM_TEST_TYPE_INVALID;
    test->funcs = &empty_funcs;
    pm->num_tests++;

exit:
    return test;
}

void pm_release_test(test_t* test) {
    when_null_trace(test, exit, WARNING, "Test is NULL");

    SAH_TRACEZ_INFO(ME, "release test[%s]", test->name);

    if(test->funcs->release != NULL) {
        test->funcs->release(test);
    }

    if(amxp_subproc_is_running(test->fail_process)) {
        amxp_subproc_kill(test->fail_process, SIGKILL);
        if(amxp_subproc_wait(test->fail_process, 2000) != 0) {
            SAH_TRACEZ_WARNING(ME, "Failed to kill FailAction process for test '%s'", test->name);
        }
    }
    amxp_subproc_delete(&(test->fail_process));

    amxp_timer_delete(&(test->timer_check));

    if(test->valid_conf) {
        test->valid_conf = false;
        test->pm->num_valid_tests--;
        pm_handle_test_update(test->pm, test);
    }

    amxc_llist_it_take(&(test->it));
    test->pm->num_tests--;

    free(test->name);
    free(test->subject);
    free(test->fail_action);
    free(test);
    test = NULL;

exit:
    return;
}

bool pm_check_if_perform_test(test_t* test) {
    time_t now;
    time_t last_check;
    time_t successful_since;
    struct tm tm_last_check;
    struct tm tm_successful_since;
    bool perform_positive_test = false;
    bool perform_test = false;
    bool ret = false;

    when_null_trace(test, exit, WARNING, "Test is NULL");

    // check if the test should be performed
    time(&now);
    amxc_ts_to_tm_utc(&test->last_check, &tm_last_check);
    last_check = timegm(&tm_last_check);

    // Change last_check to 0 if the value is "invalid" ( < 0 or > current time )
    if((last_check < 0) || (last_check > now)) {
        last_check = 0;
    }

    amxc_ts_to_tm_utc(&test->successful_since, &tm_successful_since);
    successful_since = timegm(&tm_successful_since);

    // after executing a FailAction we will check if the FailAction was successful
    if(test->fail_action_executed && !amxp_subproc_is_running(test->fail_process)) {
        perform_positive_test = true;
    }

    // check if the test_interval should be reset
    if(test->result &&
       (difftime(now, successful_since) >= test->test_reset_interval)) {
        SAH_TRACEZ_INFO(ME, "check for reset");
        perform_positive_test = true;
    }

    // if the current_test_interval is expired then a full test will be performed
    if(difftime(now, last_check) >= test->current_test_interval) {
        perform_test = true;
    }

    // if the max_fail_duration is expired when the test is failing then a full test will be performed
    if((test->max_fail_duration >= 0) && (test->num_failed > 0)) {
        struct tm tm_failed_since;
        time_t failed_since;

        amxc_ts_to_tm_utc(&test->failed_since, &tm_failed_since);
        failed_since = timegm(&tm_failed_since);

        if(difftime(now, failed_since) >= test->max_fail_duration) {
            perform_test = true;
        }
    }

    when_true(!perform_positive_test && !perform_test, exit);

    test->only_check_positive = perform_positive_test && !perform_test;
    ret = true;

exit:
    return ret;
}

void pm_perform_test(test_t* test) {
    SAH_TRACEZ_INFO(ME, "Performing test '%s'", test->name);

    if(test->active) {
        SAH_TRACEZ_WARNING(ME, "Test '%s' still active", test->name);
        return;
    }

    if(!pm_check_if_perform_test(test)) {
        return;
    }

    test->active = true;
    test->has_result = false;
    test->result = false;
    test->fail_action_executed = false;

    if(test->funcs->perform != NULL) {
        if(!test->funcs->perform(test)) {
            test->has_result = true;
        }
    } else {
        test->has_result = true;
    }

    if(test->has_result) {
        pm_handle_result(test);
    }
}

static void pm_update_test_status_ok(test_t* test, const char* health, amxc_ts_t empty_converted, amxc_ts_t now_converted, amxd_trans_t* trans) {
    time_t successful_since;
    struct tm tm_successful_since;
    time_t now;
    struct tm tm_now;

    if(strcmp(health, "Bad") == 0) {
        test->recovery = true;
    }

    if((health == NULL) || strcmp(health, "Good")) {
        amxd_trans_set_value(cstring_t, trans, "Health", "Good");
        amxd_trans_set_value(amxc_ts_t, trans, "SuccessfulSince", &now_converted);
        amxd_trans_set_value(amxc_ts_t, trans, "FailedSince", &empty_converted);
        test->failed_since = empty_converted;
        test->successful_since = now_converted;
    }
    amxd_trans_set_value(amxc_ts_t, trans, "LastSuccess", &now_converted);
    amxd_trans_set_value(uint32_t, trans, "NumFailed", 0);
    test->num_failed = 0;

    amxd_trans_set_value(uint32_t, trans, "NumFailActions", 0);
    amxd_trans_set_value(cstring_t, trans, "LastFailReason", PM_TEST_FAIL_REASON_TEST_OK);

    amxc_ts_to_tm_utc(&test->successful_since, &tm_successful_since);
    successful_since = timegm(&tm_successful_since);

    amxc_ts_to_tm_utc(&now_converted, &tm_now);
    now = timegm(&tm_now);

    if(difftime(now, successful_since) >= test->test_reset_interval) {
        // reset test interval
        SAH_TRACEZ_INFO(ME, "'%s' reset test interval", test->name);
        test->recovery = false;
        update_current_test_interval(test);
        amxd_trans_set_value(uint32_t, trans, "CurrentTestInterval", test->current_test_interval);
    }
}

static void pm_update_test_status_ko(test_t* test, const char* health, amxc_ts_t empty_converted, amxc_ts_t now_converted, amxd_trans_t* trans) {
    const char* fail_reason = NULL;

    if((health == NULL) || strcmp(health, "Bad")) {
        amxd_trans_set_value(cstring_t, trans, "Health", "Bad");
        amxd_trans_set_value(amxc_ts_t, trans, "FailedSince", &now_converted);
        amxd_trans_set_value(amxc_ts_t, trans, "SuccessfulSince", &empty_converted);
        test->failed_since = now_converted;
        test->successful_since = empty_converted;
    }
    test->num_failed++;
    amxd_trans_set_value(uint32_t, trans, "NumFailed", amxd_object_get_value(uint32_t, test->object, "NumFailed", NULL) + 1);


    // update test interval
    update_current_test_interval(test);
    amxd_trans_set_value(uint32_t, trans, "CurrentTestInterval", test->current_test_interval);

    fail_reason = pm_check_test(test);
    if(!STR_EMPTY(fail_reason)) {
        amxd_trans_set_value(cstring_t, trans, "LastFailReason", fail_reason);
    }
}

static void pm_update_test_status(test_t* test, const char* health, amxc_ts_t empty_converted, amxc_ts_t now_converted) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, test->object);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    if(test->result) {
        pm_update_test_status_ok(test, health, empty_converted, now_converted, &trans);
    } else {
        pm_update_test_status_ko(test, health, empty_converted, now_converted, &trans);
    }

    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }

    amxd_trans_clean(&trans);
}

void pm_handle_result(test_t* test) {
    const char* health = NULL;
    amxc_ts_t empty_converted = {0, 0, 0};
    amxc_ts_t now_converted = {0, 0, 0};
    amxd_trans_t trans;

    when_null_trace(test, exit, WARNING, "Test is NULL");

    SAH_TRACEZ_INFO(ME, "Test[%s]: %s", test->name, test->has_result ? (test->result ? "is OK" : "is NOK") : "No Result");

    if(!test->active || !test->has_result) {
        SAH_TRACEZ_WARNING(ME, "Test '%s' not active or no result", test->name);
        return;
    }
    test->active = false;

    if(test->only_check_positive && !test->result) {
        SAH_TRACEZ_INFO(ME, "'%s': only positive tests will be processed", test->name);
        return;
    }

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, test->object);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxc_ts_now(&now_converted);

    amxd_trans_set_value(amxc_ts_t, &trans, "LastCheck", &now_converted);
    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }

    test->last_check = now_converted;
    health = amxc_var_constcast(cstring_t, amxd_object_get_param_value(test->object, "Health"));
    pm_update_test_status(test, health, empty_converted, now_converted);

    amxd_trans_clean(&trans);

exit:
    return;
}

void _pm_test_added(UNUSED const char* const sig_name,
                    const amxc_var_t* const data,
                    UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    processmonitor_t* pm = pm_get_process_monitor();
    test_t* test = NULL;
    amxd_object_t* object = amxd_dm_signal_get_object(get_dm(), data);
    amxd_object_t* test_instance_object = NULL;

    when_null_trace(object, exit, ERROR, "Data model object from signal %s is NULL", sig_name);
    test_instance_object = amxd_object_get_instance(object, NULL, GET_UINT32(data, "index"));

    test = pm_create_test(pm, test_instance_object);
    when_null(test, exit);

    pm_update_int32_value(&test->type, pm_str2type(amxc_var_constcast(cstring_t, amxd_object_get_param_value(test->object, "Type"))));
    pm_update_char_value(&test->subject, amxc_var_constcast(cstring_t, amxd_object_get_param_value(test->object, "Subject")));
    pm_update_int32_value(&test->max_fail_num, amxd_object_get_value(int32_t, test->object, "MaxFailNum", NULL));
    pm_update_int32_value(&test->max_fail_duration, amxd_object_get_value(int32_t, test->object, "MaxFailDuration", NULL));
    pm_update_char_value(&test->fail_action, amxc_var_constcast(cstring_t, amxd_object_get_param_value(test->object, "FailAction")));
    pm_update_uint32_value(&test->test_interval, amxd_object_get_value(int32_t, test->object, "TestInterval", NULL));
    pm_update_uint32_value(&test->test_interval_multiplier, amxd_object_get_value(uint32_t, test->object, "TestIntervalMultiplier", NULL));
    pm_update_int32_value(&test->test_reset_interval, amxd_object_get_value(int32_t, test->object, "TestResetInterval", NULL));
    pm_update_uint32_value(&test->reboot_after_restart_threshold, amxd_object_get_value(uint32_t, test->object, "RebootAfterRestartThreshold", NULL));

    test->active = false;

    switch(test->type) {
    case PM_TEST_TYPE_PLUGIN:
        pm_set_plugin_functions(test);
        break;
    case PM_TEST_TYPE_PROCESS:
        pm_set_process_functions(test);
        break;
    case PM_TEST_TYPE_CUSTOM:
        pm_set_custom_functions(test);
        break;
    default:
        test->funcs = &empty_funcs;
        break;
    }

    if(test->funcs->provide != NULL) {
        test->funcs->provide(test);
    }

    pm_validate_test(test);
    pm_validate_tests(&(test->pm->tests));

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void pm_reload_test(test_t* test, int old_type) {
    if(test->active) {
        if(test->funcs->stop != NULL) {
            test->funcs->stop(test);
        }

        test->active = false;
    }

    if(old_type != test->type) {
        if(test->funcs->release != NULL) {
            test->funcs->release(test);
        }

        switch(test->type) {
        case PM_TEST_TYPE_PLUGIN:
            pm_set_plugin_functions(test);
            break;
        case PM_TEST_TYPE_PROCESS:
            pm_set_process_functions(test);
            break;
        case PM_TEST_TYPE_CUSTOM:
            pm_set_custom_functions(test);
            break;
        default:
            test->funcs = &empty_funcs;
            break;
        }

        if(test->funcs->provide != NULL) {
            test->funcs->provide(test);
        }
    }

    pm_validate_test(test);
}

void _pm_test_changed(UNUSED const char* const sig_name,
                      const amxc_var_t* const data,
                      UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    bool changed = false;
    int old_type = 0;
    test_t* test = NULL;
    amxd_object_t* object = amxd_dm_signal_get_object(get_dm(), data);

    when_null_trace(object, exit, ERROR, "Data model object from signal %s is NULL", sig_name);

    test = object->priv;

    if(test == NULL) {
        processmonitor_t* pm = pm_get_process_monitor();
        test = pm_create_test(pm, object);
        when_null(test, exit);
        SAH_TRACEZ_INFO(ME, "Test created");
    }

    old_type = test->type;
    changed |= pm_update_int32_value(&test->type, pm_str2type(amxc_var_constcast(cstring_t, amxd_object_get_param_value(test->object, "Type"))));
    changed |= pm_update_char_value(&test->subject, amxc_var_constcast(cstring_t, amxd_object_get_param_value(test->object, "Subject")));
    changed |= pm_update_int32_value(&test->max_fail_num, amxd_object_get_value(int32_t, test->object, "MaxFailNum", NULL));
    changed |= pm_update_int32_value(&test->max_fail_duration, amxd_object_get_value(int32_t, test->object, "MaxFailDuration", NULL));
    changed |= pm_update_char_value(&test->fail_action, amxc_var_constcast(cstring_t, amxd_object_get_param_value(test->object, "FailAction")));
    changed |= pm_update_uint32_value(&test->test_interval, amxd_object_get_value(int32_t, test->object, "TestInterval", NULL));
    changed |= pm_update_uint32_value(&test->test_interval_multiplier, amxd_object_get_value(uint32_t, test->object, "TestIntervalMultiplier", NULL));
    changed |= pm_update_int32_value(&test->test_reset_interval, amxd_object_get_value(int32_t, test->object, "TestResetInterval", NULL));
    changed |= pm_update_uint32_value(&test->reboot_after_restart_threshold, amxd_object_get_value(uint32_t, test->object, "RebootAfterRestartThreshold", NULL));

    if(changed) {
        pm_reload_test(test, old_type);
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _pm_test_removed(UNUSED const char* const sig_name,
                      UNUSED const amxc_var_t* const data,
                      UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);

    processmonitor_t* pm = pm_get_process_monitor();
    pm_validate_tests(&(pm->tests));

    SAH_TRACEZ_OUT(ME);
    return;
}

void _pm_changed(UNUSED const char* const sig_name,
                 UNUSED const amxc_var_t* const data,
                 UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);

    processmonitor_t* pm = pm_get_process_monitor();
    pm_validate_tests(&(pm->tests));

    SAH_TRACEZ_OUT(ME);
    return;
}

amxd_status_t _pm_delete_test(amxd_object_t* object,
                              amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_ok;
    amxd_object_t* instance = NULL;

    if(reason != action_object_del_inst) {
        status = amxd_status_function_not_implemented;
        goto exit;
    }

    when_false(amxc_var_type_of(args) == AMXC_VAR_ID_HTABLE, exit);
    when_null(GET_ARG(args, "index"), exit);
    instance = amxd_object_get_instance(object, NULL, GET_UINT32(args, "index"));

    pm_release_test((test_t*) instance->priv);

    status = amxd_action_object_del_inst(object, param, reason, args, retval, priv);

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}
