/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <math.h>

#include "processmonitor.h"

static processmonitor_t processmonitor;

amxd_dm_t* get_dm(void) {
    return processmonitor.dm;
}

amxo_parser_t* processmonitor_get_parser(void) {
    return processmonitor.parser;
}

static void pm_reboot(const char* reason) {
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    int rv = -1;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);

    bus_ctx = amxb_be_who_has("Device.");
    when_null_trace(bus_ctx, exit, ERROR, "Failed to get the context bus.");

    amxc_var_add_key(cstring_t, &args, "Cause", "LocalReboot");
    amxc_var_add_key(cstring_t, &args, "Reason", reason);
    rv = amxb_call(bus_ctx, "Device.", "Reboot", &args, &ret, 5);
    when_failed_trace(rv, exit, ERROR, "Failed to call Device.Reboot() for object: %d", rv);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return;
}

static void pm_do_reboot_debug(processmonitor_t* pm) {
    struct stat statbuf;
    time_t now;
    struct tm reboot_start;

    time(&now);
    amxc_ts_to_tm_utc(&pm->reboot_start, &reboot_start);

    if(!stat(PM_DEBUG_INFORMATION_FILE, &statbuf)) {
        SAH_TRACEZ_ERROR(ME, "Debug information is in, executing NMC reboot.");

        pm_reboot("ProcessMonitor");
        pm->reboot_stage = PM_REBOOT_STAGE_NMC;
        amxp_timer_start(pm->reboot, PM_REBOOT_STAGE_TIMEOUT);

        SAH_TRACEZ_ERROR(ME, "System entered the NetModeConfig Reboot stage");
    } else if(now >= timegm(&reboot_start) + PM_GET_DEBUG_MAX_TIMEOUT) {
        SAH_TRACEZ_ERROR(ME, "Debug information not yet in after %d seconds, executing NMC reboot.", PM_GET_DEBUG_MAX_TIMEOUT);

        pm_reboot("ProcessMonitor");
        pm->reboot_stage = PM_REBOOT_STAGE_NMC;
        amxp_timer_start(pm->reboot, PM_REBOOT_STAGE_TIMEOUT);

        SAH_TRACEZ_ERROR(ME, "System entered the NetModeConfig Reboot stage");
    } else {
        SAH_TRACEZ_ERROR(ME, "Debug information not yet in, waiting ...");
        amxp_timer_start(pm->reboot, PM_REBOOT_DEBUG_TIMEOUT);
    }
}

static void pm_do_reboot_stage(UNUSED amxp_timer_t* timer, void* priv) {
    processmonitor_t* pm = (processmonitor_t*) priv;

    when_null_trace(pm, exit, ERROR, "Programming error, unexpected NULL pointer");

    switch(pm->reboot_stage) {
    case PM_REBOOT_STAGE_DEBUG:
        pm_do_reboot_debug(pm);

        break;
    case PM_REBOOT_STAGE_NMC:
        SAH_TRACEZ_ERROR(ME, "System still active after NetModeConfig reboot, trying with Shell reboot");

        if(system("reboot") < 0) {
            SAH_TRACEZ_ERROR(ME, "Error executing the reboot command");
        }

        pm->reboot_stage = PM_REBOOT_STAGE_SHELL;
        amxp_timer_start(pm->reboot, PM_REBOOT_STAGE_TIMEOUT);

        SAH_TRACEZ_ERROR(ME, "System entered the Shell Reboot stage");
        break;
    default:
        break;
    }

exit:
    return;
}

static void pm_update_test_interval(processmonitor_t* pm) {
    uint32_t old = 0;
    uint32_t diff = 0;
    uint32_t rem = 0;
    uint32_t cycle_duration = 0;

    when_null_trace(pm, exit, ERROR, "PM is NULL");
    when_null_trace(pm->object, exit, ERROR, "PM object is NULL");

    cycle_duration = amxd_object_get_value(uint32_t, pm->object, "CycleDuration", NULL);

    old = pm->test_interval;

    if(pm->num_valid_tests == 0) {
        pm->test_interval = 0;
    } else {
        pm->test_interval = ceil((double) cycle_duration / pm->num_valid_tests);
    }

    if(pm->testing_active && (amxp_timer_get_state(pm->timer_test) == amxp_timer_running) && (pm->test_interval < old)) {
        diff = (old - pm->test_interval) * 1000;
        rem = amxp_timer_remaining_time(pm->timer_test);

        if(rem > diff) {
            amxp_timer_start(pm->timer_test, rem - diff);
        } else {
            amxp_timer_start(pm->timer_test, 0);
        }
    }

exit:
    return;
}

processmonitor_t* pm_get_process_monitor(void) {
    return &processmonitor;
}

bool pm_select_next_test(processmonitor_t* pm) {
    bool res = false;
    amxc_llist_it_t* test = NULL;

    if(pm->num_valid_tests == 0) {
        pm->next_test = NULL;
        goto exit;
    }

    if(pm->next_test != NULL) {
        test = amxc_llist_it_get_next(pm->next_test);
    } else {
        test = amxc_llist_get_first(&(pm->tests));
    }

    // move to next valid test or end of list (test == NULL)
    for(; test && !(((test_t*) amxc_container_of(test, test_t, it))->valid_conf); test = amxc_llist_it_get_next(test)) {
    }

    // if end of list, start from the beginning and search for a valid test
    if(test == NULL) {
        for(test = amxc_llist_get_first(&(pm->tests)); test && !(((test_t*) amxc_container_of(test, test_t, it))->valid_conf); test = amxc_llist_it_get_next(test)) {
        }
    }

    pm->next_test = test;
    when_null_trace(test, exit, ERROR, "Programming error - There should be a valid test, but I can't find it!");

    res = true;

exit:
    return res;
}

bool pm_handle_test_update(processmonitor_t* pm, test_t* test) {
    bool res = false;

    when_null(pm->timer_test, exit);

    // update valid tests
    pm->num_valid_tests = 0;
    amxc_llist_for_each(lit, &(pm->tests)) {
        test_t* temp_test = amxc_llist_it_get_data(lit, test_t, it);
        if(temp_test->valid_conf != 0) {
            pm->num_valid_tests++;
        }
    }

    pm_update_test_interval(pm);

    if((&(test->it) == pm->next_test) && (test->valid_conf == false)) {
        pm_select_next_test(pm);
    }

    if(pm->testing_active && !pm->num_valid_tests) {
        SAH_TRACEZ_NOTICE(ME, "No valid configurations, stop testing");
        amxp_timer_stop(pm->timer_test);
        pm->testing_active = false;
    } else if(!pm->testing_active && pm->num_valid_tests) {
        SAH_TRACEZ_NOTICE(ME, "Valid configurations, start testing");
        if(pm->next_test == NULL) {
            pm_select_next_test(pm);
        }

        amxp_timer_start(pm->timer_test, 0);
        pm->testing_active = true;
    }
    res = true;

exit:
    return res;
}

void pm_perform_reboot(processmonitor_t* pm, const char* reason, amxc_ts_t ts_now) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    when_true_trace(pm->reboot_stage != PM_REBOOT_STAGE_NONE, exit, ERROR, "Got new reboot request, but system is already in reboot stage");

    amxd_trans_select_object(&trans, pm->object);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    /* Stop the tests! */
    amxp_timer_stop(pm->timer_test);

    amxd_trans_set_value(amxc_ts_t, &trans, "LastReboot", &ts_now);
    amxd_trans_set_value(cstring_t, &trans, "RebootReason", reason);

    if(amxd_trans_apply(&trans, get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }

    SAH_TRACEZ_ERROR(ME, "Rebooting the system: '%s'", reason);
    SAH_TRACEZ_ERROR(ME, "Retrieving debug information...");

    unlink(PM_DEBUG_INFORMATION_FILE);
    if(system("(/bin/getDebugInformation " PM_DEBUG_INFORMATION_FILE ".tmp; mv -f " PM_DEBUG_INFORMATION_FILE ".tmp " PM_DEBUG_INFORMATION_FILE ") &") < 0) {
        SAH_TRACEZ_ERROR(ME, "Error executing the getDebugInformation command");
    }

    pm->reboot_stage = PM_REBOOT_STAGE_DEBUG;
    pm->reboot_start = ts_now;
    amxp_timer_start(pm->reboot, PM_REBOOT_DEBUG_TIMEOUT);

    SAH_TRACEZ_ERROR(ME, "System entered the Debug Reboot stage");

exit:
    amxd_trans_clean(&trans);
}

void pm_do_test(amxp_timer_t* timer, void* priv) {
    processmonitor_t* pm = (processmonitor_t*) priv;

    when_null_trace(pm, exit, ERROR, "Programming error, unexpected NULL pointer");
    when_null_trace(pm->next_test, exit, ERROR, "Programming error, unexpected NULL pointer");

    pm_perform_test(amxc_container_of(pm->next_test, test_t, it));

    pm_select_next_test(pm);

    amxp_timer_start(timer, pm->test_interval * 1000);

exit:
    return;
}

static int32_t initialize(amxd_dm_t* dm, amxo_parser_t* parser) {
    int32_t res = -1;
    processmonitor.dm = dm;
    processmonitor.parser = parser;
    processmonitor.object = amxd_dm_get_object(dm, "ProcessMonitor");

    amxc_llist_init(&(processmonitor.tests));

    when_failed_trace(amxp_timer_new(&processmonitor.timer_test, pm_do_test, &processmonitor), exit, ERROR, "Failed to create timer for do_test");
    when_failed_trace(amxp_timer_new(&processmonitor.reboot, pm_do_reboot_stage, &processmonitor), exit, ERROR, "Failed to create timer for do_reboot_stage");

    res = 0;

exit:
    return res;
}

static void test_delete(amxc_llist_it_t* it) {
    test_t* test = amxc_container_of(it, test_t, it);
    pm_release_test(test);
}

static int32_t cleanup(void) {
    amxp_timer_delete(&(processmonitor.timer_test));
    amxp_timer_delete(&(processmonitor.reboot));
    amxc_llist_clean(&(processmonitor.tests), test_delete);
    processmonitor.dm = NULL;
    processmonitor.parser = NULL;
    return 0;
}

amxd_status_t _pm_write_cycle_duration(amxd_object_t* object,
                                       amxd_param_t* param,
                                       amxd_action_t reason,
                                       const amxc_var_t* const args,
                                       amxc_var_t* const retval,
                                       void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_ok;
    processmonitor_t* pm = pm_get_process_monitor();

    when_true_status(reason != action_param_write, exit, status = amxd_status_function_not_implemented);

    // write handler data back to dm object before manipulating it
    status = amxd_action_param_write(object, param, reason, args, retval, priv);

    pm_update_test_interval(pm);

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _pm_read_test_interval(UNUSED amxd_object_t* object,
                                     UNUSED amxd_param_t* param,
                                     amxd_action_t reason,
                                     UNUSED const amxc_var_t* const args,
                                     amxc_var_t* const retval,
                                     UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_ok;
    processmonitor_t* pm = pm_get_process_monitor();

    when_true_status(reason != action_param_read, exit, status = amxd_status_function_not_implemented);

    amxc_var_set_uint32_t(retval, (uint32_t) (pm->test_interval));
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

int _processmonitor_main(int reason,
                         amxd_dm_t* dm,
                         amxo_parser_t* parser) {
    int retval = 0;
    switch(reason) {
    case 0:     // START
        SAH_TRACEZ_INFO(ME, "Starting %s", ME);
        retval = initialize(dm, parser);
        break;
    case 1:     // STOP
        SAH_TRACEZ_INFO(ME, "Stoping %s", ME);
        retval = cleanup();
        break;
    }

    return retval;
}
