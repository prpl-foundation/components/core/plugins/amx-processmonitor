/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _POSIX_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <signal.h>

#include "processmonitor.h"

static bool pm_test_pid(test_t* test) {
    if(!test->pid->have_pid) {
        SAH_TRACEZ_WARNING(ME, "Programming error: no PID provided");
        return false;
    }

    /*
     * Call kill() with 'sig' set to 0, this doesn't send a signal
     * but does perform the checks, like whether the process exists.
     */
    if(!kill(test->pid->pid, 0)) {
        return true;
    }

    if(errno == ESRCH) {
        SAH_TRACEZ_WARNING(ME, "No process with PID %d exists", test->pid->pid);
    } else {
        SAH_TRACEZ_WARNING(ME, "Unable to check PID %d: %s", test->pid->pid, strerror(errno));
    }

    return false;
}

static bool pm_has_pid_from_file(test_t* test) {
    int pid = 0;
    FILE* fin = NULL;

    fin = fopen(test->subject, "r");
    if(fin != NULL) {
        if((fscanf(fin, "%d", &pid) == 1) && (pid >= 1)) {
            SAH_TRACEZ_INFO(ME, "Got PID '%d' from file '%s' for process test '%s'", pid, test->subject, test->name);
            test->pid->have_pid = true;
            test->pid->pid = pid;
        }

        fclose(fin);
    }

    return test->pid->have_pid;
}

static bool pm_has_pid_from_text(test_t* test) {
    long int pid = 0;
    char* endptr = NULL;

    errno = 0;
    pid = strtol(test->subject, &endptr, 10);
    if((*endptr == '\0') && (errno == 0) && (pid >= 1)) {
        SAH_TRACEZ_INFO(ME, "Got PID '%ld' from text '%s' for process test '%s'", pid, test->subject, test->name);
        test->pid->have_pid = true;
        test->pid->pid = pid;
    }

    return test->pid->have_pid;
}

static bool pm_has_pid(test_t* test) {
    test->pid->have_pid = false;

    if(!pm_has_pid_from_text(test)) {
        pm_has_pid_from_file(test);
    }

    return test->pid->have_pid;
}

static bool pm_process_provide(test_t* test) {
    SAH_TRACEZ_INFO(ME, "Providing environment for process test '%s'", test->name);

    if(test->pid != NULL) {
        SAH_TRACEZ_ERROR(ME, "Programming error - providing process test while something is already provided");
        return false;
    }

    test->pid = calloc(1, sizeof(test_pid_t));
    if(test->pid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to allocate pid structure for process test '%s'", test->name);
        return false;
    }

    return true;
}

static void pm_process_release(test_t* test) {
    SAH_TRACEZ_INFO(ME, "Releasing environment for process test '%s'", test->name);

    free(test->pid);
    test->pid = NULL;
}

static bool pm_process_perform(test_t* test) {
    SAH_TRACEZ_NOTICE(ME, "Verifying process test '%s', PID is '%s'", test->name, test->subject);

    if(test->pid == NULL) {
        SAH_TRACEZ_ERROR(ME, "Environment is not ok to perform test");
        return false;
    }

    if(!pm_has_pid(test)) {
        SAH_TRACEZ_WARNING(ME, "Unable to retrieve PID value for process test '%s'", test->name);
        return false;
    }

    test->has_result = true;
    if(!pm_test_pid(test)) {
        SAH_TRACEZ_WARNING(ME, "Process test '%s' (PID=%d) failed verification", test->name, test->pid->pid);
        test->result = false;
    } else {
        SAH_TRACEZ_INFO(ME, "Process test '%s' (PID=%d) verified to be OK", test->name, test->pid->pid);
        test->result = true;
    }

    return true;
}

static test_functions_t process_funcs =
{
    .provide = pm_process_provide,
    .release = pm_process_release,
    .perform = pm_process_perform,
};

void pm_set_process_functions(test_t* test) {
    test->funcs = &process_funcs;
}
