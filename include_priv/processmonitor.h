/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__PROCESSMONITOR_H__)
#define __PROCESSMONITOR_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc_timestamp.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "processmonitor"
#define UNUSED __attribute__((unused))

amxd_dm_t* PRIVATE get_dm(void);
amxo_parser_t* PRIVATE processmonitor_get_parser(void);

#define CONFIG_RWDATAPATH "/cfg/system"
#define PM_DEBUG_INFORMATION_FILE   CONFIG_RWDATAPATH "/pm_debug_info.txt"
#define SCRIPT_DIR                  "/usr/lib/processmonitor/scripts/"

#define PM_TEST_TYPE_INVALID        -1
#define PM_TEST_TYPE_PLUGIN         0
#define PM_TEST_TYPE_PROCESS        1
#define PM_TEST_TYPE_CUSTOM         2

#define PM_TEST_FAIL_REASON_TEST_OK         "Error_None"
#define PM_TEST_FAIL_REASON_TEST_OCCURENCES "Error_OccurencesTestFailed"
#define PM_TEST_FAIL_REASON_TEST_TIMEOUT    "Error_DurationTestFailed"

#define PM_FAIL_ACTION_REBOOT         "REBOOT"
#define PM_FAIL_ACTION_RESTART        "RESTART"
#define PM_FAIL_ACTION_NO_ACTION      "NO_ACTION"

#define PM_REBOOT_STAGE_NONE        0
#define PM_REBOOT_STAGE_DEBUG       1
#define PM_REBOOT_STAGE_NMC         2
#define PM_REBOOT_STAGE_SHELL       3
#define PM_REBOOT_STAGE_WATCHDOG    4

#define PM_REBOOT_DEBUG_TIMEOUT     1000    // milliseconds
#define PM_REBOOT_STAGE_TIMEOUT     10000   // milliseconds

#define PM_GET_DEBUG_MAX_TIMEOUT    30      // seconds

struct processmonitor_t {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    amxd_object_t* object;
    bool testing_active;
    amxp_timer_t* timer_test;
    amxp_timer_t* reboot;
    amxc_llist_t tests;
    amxc_llist_it_t* next_test;
    unsigned int num_tests;
    unsigned int num_valid_tests;
    int reboot_stage;
    amxc_ts_t reboot_start;
    unsigned int test_interval;
};

struct test_t {
    char* name;
    amxc_llist_it_t it;
    amxd_object_t* object;
    struct processmonitor_t* pm;
    bool active;
    bool has_result;
    bool result;
    bool fail_action_executed;
    bool only_check_positive;
    union
    {
        amxb_request_t* req;
        amxp_subproc_t* process;
        struct test_pid_t* pid;
        void* none;
    };
    struct test_functions_t* funcs;
    amxp_timer_t* timer_check;
    unsigned int test_interval;
    unsigned int test_interval_multiplier;
    unsigned int current_test_interval;
    int test_reset_interval;
    uint32_t reboot_after_restart_threshold;
    amxc_ts_t last_check;
    amxc_ts_t last_success;
    amxc_ts_t failed_since;
    amxc_ts_t successful_since;
    unsigned int num_failed;
    bool valid_conf;
    int type;
    char* subject;
    int max_fail_num;
    int max_fail_duration;
    char* fail_action;
    amxp_subproc_t* fail_process;
    bool recovery;
};

struct test_pid_t {
    bool have_pid;
    pid_t pid;
};

struct test_functions_t {
    bool (* provide)(struct test_t* test);
    void (* release)(struct test_t* test);
    bool (* validate)(struct test_t* test);
    bool (* perform)(struct test_t* test);
    bool (* stop)(struct test_t* test);
    void (* timed_out)(struct test_t* test);
};

typedef struct processmonitor_t processmonitor_t;
typedef struct test_t test_t;
typedef struct test_pid_t test_pid_t;
typedef struct test_functions_t test_functions_t;

/* processmonitor.c */
int _processmonitor_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser);
processmonitor_t* pm_get_process_monitor(void);
void pm_do_test(amxp_timer_t* timer, void* priv);
bool pm_select_next_test(processmonitor_t* pm);
bool pm_handle_test_update(processmonitor_t* pm, test_t* test);
void pm_perform_reboot(processmonitor_t* pm, const char* reason, amxc_ts_t ts_now);
amxd_status_t _pm_write_cycle_duration(amxd_object_t* object,
                                       amxd_param_t* param,
                                       amxd_action_t reason,
                                       const amxc_var_t* const args,
                                       amxc_var_t* const retval,
                                       void* priv);
amxd_status_t _pm_read_test_interval(amxd_object_t* object,
                                     amxd_param_t* param,
                                     amxd_action_t reason,
                                     const amxc_var_t* const args,
                                     amxc_var_t* const retval,
                                     void* priv);
amxd_status_t _pm_delete_test(amxd_object_t* object,
                              amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv);

void _pm_test_added(UNUSED const char* const sig_name,
                    const amxc_var_t* const data,
                    UNUSED void* const priv);
void _pm_test_removed(UNUSED const char* const sig_name,
                      const amxc_var_t* const data,
                      UNUSED void* const priv);
void _pm_test_changed(UNUSED const char* const sig_name,
                      const amxc_var_t* const data,
                      UNUSED void* const priv);
void _pm_changed(UNUSED const char* const sig_name,
                 const amxc_var_t* const data,
                 UNUSED void* const priv);

/* test.c */
void pm_release_test(test_t* test);
bool pm_check_if_perform_test(test_t* test);
void pm_perform_test(test_t* test);
void pm_handle_result(test_t* test);

/* plugin.c */
void pm_set_plugin_functions(test_t* test);

/* process.c */
void pm_set_process_functions(test_t* test);

/* custom.c */
void pm_set_custom_functions(test_t* test);

#ifdef __cplusplus
}
#endif

#endif // __PROCESSMONITOR_H__
